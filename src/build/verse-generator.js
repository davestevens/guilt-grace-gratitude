const { globSync } = require('glob')
const fs = require('fs')

const pageFiles = globSync('./src/app/**/page.tsx')
const verses = require('kjv/json/verses-1769.json')

function range(start, maybeEnd = null) {
  const end = maybeEnd ? maybeEnd : start
  return Array(end - start + 1)
    .fill()
    .map((_, idx) => start + idx)
}

function getReferences() {
  const refMap = {}
  const search = /{ b: '(.*)', c: [0-9]*, vf: [0-9]*(?: })?(?:, vt: [0-9]* })?/g
  for (const filename of pageFiles) {
    const fileData = fs.readFileSync(filename).toString()
    const refs = fileData.match(search)
    if (refs) {
      for (const refUnparsed of refs) {
        const jsonStr = refUnparsed
          .replace('b:', '"b":')
          .replaceAll("'", '"')
          .replace('c:', '"c":')
          .replace('vf:', '"vf":')
          .replace('vt:', '"vt":')
        const ref = JSON.parse(jsonStr)
        const newVerses = range(ref.vf, ref.vt)
        if (refMap[ref.b]) {
          const book = refMap[ref.b]
          if (book[ref.c]) {
            const chapter = book[ref.c]
            book[ref.c] = Array.from(new Set([...chapter, ...newVerses]))
          } else {
            // new chapter
            book[ref.c] = newVerses
          }
        } else {
          // new book
          const book = {}
          book[ref.c] = newVerses
          refMap[ref.b] = book
        }
      }
    }
  }
  return refMap
}

module.exports.gatherPassages = function gatherPassages() {
  const refMap = getReferences()
  const passages = {}
  for (const book of Object.keys(refMap)) {
    for (const chapter of Object.keys(refMap[book])) {
      for (const verse of refMap[book][chapter]) {
        if (!passages[book]) {
          passages[book] = {}
        }
        if (!passages[book][chapter]) {
          passages[book][chapter] = {}
        }
        passages[book][chapter][verse] = verses[`${book} ${chapter}:${verse}`]
      }
    }
  }
  return passages
}

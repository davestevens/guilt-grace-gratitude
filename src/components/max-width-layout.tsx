import React from 'react'
import { HeroUIProvider } from '@heroui/react'
import Header from '@/components/header'
import Footer from '@/components/footer'
import { Open_Sans } from 'next/font/google'

const openSans = Open_Sans({ weight: '300', subsets: ['latin'] })

export default function MaxWidthLayout({
  children,
}: Readonly<{ children: React.ReactNode }>) {
  return (
    <html lang="en" className="dark">
      <body
        className={`${openSans.className} dark bg-zinc-950 text-slate-50 flex flex-col w-full min-h-screen`}
      >
        <HeroUIProvider className="flex flex-col items-center flex-1">
          <main className="flex flex-col items-center max-w-3xl w-full">
            <Header />
            <div className="flex flex-col w-full items-center p-4">
              {children}
            </div>
          </main>
          <Footer />
        </HeroUIProvider>
      </body>
    </html>
  )
}

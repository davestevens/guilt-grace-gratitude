import type { Metadata } from 'next'
import './globals.css'

export const metadata: Metadata = {
  title: 'Guilt Grace Gratitude',
  description:
    'Guilt Grace Gratitude is an illustrated edition of the Heidelberg Catechism.',
}

export default function RootLayout({
  children,
}: Readonly<{ children: React.ReactNode }>) {
  return <>{children}</>
}

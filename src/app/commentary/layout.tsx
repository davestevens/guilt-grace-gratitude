import type { Metadata } from 'next'
import '../globals.css'

export const metadata: Metadata = {
  title: 'Commentary - Guilt Grace Gratitude',
  description:
    'A commentary on the Heidelberg Catechism by Dr. Zacharias Ursinus, readable on the web and as a downloadable EPUB book.',
}

export default function RootLayout({
  children,
}: Readonly<{ children: React.ReactNode }>) {
  return <>{children}</>
}

import React from 'react'
import { Link } from '@heroui/react'

export default function Commentary({
  f,
  q,
}: Readonly<{ f: string; q: string }>) {
  return (
    <div className="flex mt-3">
      <Link
        href={`/commentary?f=${f}&q=${q}`}
        rel="noopener noreferrer"
        target="_blank"
        className="text-xs ml-9"
        title="Commentary on the Heidelberg Catechism by Zacharias Ursinus"
        showAnchorIcon
      >
        Commentary
      </Link>
    </div>
  )
}

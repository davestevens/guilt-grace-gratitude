import type { Metadata } from 'next'
import '../globals.css'

export const metadata: Metadata = {
  title: 'Part 2: Grace - Guilt Grace Gratitude',
  description:
    'Part 2 of the Heidelberg Catechism describing the good news of how we can be delivered from eternal punishment',
}

export default function RootLayout({
  children,
}: Readonly<{ children: React.ReactNode }>) {
  return <>{children}</>
}

import React from 'react'

export default function LordsDay({ day }: Readonly<{ day: number }>) {
  return (
    <div className="flex mb-2">
      <div className="text-xs leading-none uppercase text-slate-400">
        Lord’s Day {day}
      </div>
    </div>
  )
}

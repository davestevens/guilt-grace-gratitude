'use client'

import React from 'react'
import sinsWagesImage from '../../../public/images/sins_wages.webp'
import meritBadgeImage from '../../../public/images/merit_badge.webp'
import graceImage from '../../../public/images/grace.webp'
import sacrificeImage from '../../../public/images/sacrifice.webp'
import twoNaturesImage from '../../../public/images/two_natures.webp'
import spotlessLambImage from '../../../public/images/spotless_lamb.webp'
import bearingWrathImage from '../../../public/images/bearing_gods_wrath.webp'
import redemptionImage from '../../../public/images/redemption.webp'
import goodNewsImage from '../../../public/images/good_news.webp'
import oliveTreeImage from '../../../public/images/olive_tree.webp'
import trustImage from '../../../public/images/hearty_trust.webp'
import athanasiusImage from '../../../public/images/athanasius_of_alexandria.webp'
import apostlesCreedImage from '../../../public/images/apostles_creed.webp'
import trinityImage from '../../../public/images/trinity.webp'
import creationImage from '../../../public/images/creation.webp'
import birdsImage from '../../../public/images/birds.webp'
import bountyAndFamineImage from '../../../public/images/bounty_and_famine.webp'
import thankfulInProsperityImage from '../../../public/images/thankful_in_prosperity.webp'
import josephsDreamImage from '../../../public/images/josephs_dream.webp'
import christPlusImage from '../../../public/images/christ_plus.webp'
import anointedImage from '../../../public/images/anointed.webp'
import professImage from '../../../public/images/profess.webp'
import adoptedImage from '../../../public/images/adopted.webp'
import crownOfThornsImage from '../../../public/images/crown_of_thorns.webp'
import maryAndJosephImage from '../../../public/images/mary_and_joseph.webp'
import clothedInRighteousnessImage from '../../../public/images/clothed_in_righteousness.webp'
import crossAndHammerImage from '../../../public/images/transom_beam_and_hammer.webp'
import pontiusPilotImage from '../../../public/images/pontius_pilate.webp'
import accursedOfGodImage from '../../../public/images/accursed_of_god.webp'
import withASpearImage from '../../../public/images/with_a_spear.webp'
import aloeAndMyrrhImage from '../../../public/images/aloe_and_myrrh.webp'
import graveyardImage from '../../../public/images/graveyard.webp'
import oldManImage from '../../../public/images/the_old_man.webp'
import flamesOfHellImage from '../../../public/images/flames_of_hell.webp'
import ascensionImage from '../../../public/images/ascension.webp'
import preachingImage from '../../../public/images/preaching.webp'
import pentecostImage from '../../../public/images/pentecost.webp'
import kingOfKingsImage from '../../../public/images/king_of_kings.webp'
import defendsTheChurchImage from '../../../public/images/defends_the_church.webp'
import upliftedHeadImage from '../../../public/images/uplifted_head.webp'
import sealedImage from '../../../public/images/sealed.webp'
import multiplyAsStarsImage from '../../../public/images/multiply_as_the_stars.webp'
import communionOfSaintsImage from '../../../public/images/communion_of_saints.webp'
import paidInFullImage from '../../../public/images/paid_in_full.webp'
import resurrectedBodyImage from '../../../public/images/resurrected_body.webp'
import eyeHasNotSeenImage from '../../../public/images/what_eye_has_not_seen.webp'
import SliderLayout from '@/components/slider-layout'
import QuestionAnswerGroup from '@/components/question-answer-group'
import Question from '@/components/question'
import Answer from '@/components/answer'
import BiblePopover from '@/components/bible-popover'
import Commentary from '@/components/commentary'
import LordsDay from '@/components/lords-day'

export default function Grace() {
  const minPage = 12
  const maxPage = 91
  const [page, setPage] = React.useState(minPage)

  return (
    <SliderLayout
      title="Part 2: Grace"
      minPage={minPage}
      maxPage={maxPage}
      page={page}
      setPage={setPage}
      prevSection="Part 1: Guilt"
      prevSectionMobile="1: Guilt"
      prevRoute="/guilt"
      nextSection="Part 3: Gratitude"
      nextSectionMobile="3: Gratitude"
      nextRoute="/gratitude"
    >
      <QuestionAnswerGroup
        image={sinsWagesImage}
        currentPage={page}
        showOnPage={12}
        key={12}
        imageAlt="Sin’s Wages"
      >
        <LordsDay day={5} />
        <Question>
          Since, then, by the righteous judgment of God we deserve temporal and
          eternal punishment, how may we escape this punishment and be again
          received into favor?
        </Question>
        <Answer>
          God wills that His justice be satisfied;
          <BiblePopover
            refs={[
              { b: 'Exodus', c: 20, vf: 5 },
              { b: 'Exodus', c: 23, vf: 7 },
            ]}
          />
          therefore, we must make full satisfaction to that justice, either by
          ourselves or by another.
          <BiblePopover refs={[{ b: 'Romans', c: 8, vf: 3, vt: 4 }]} />
        </Answer>
        <Commentary f="004" q="012" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={meritBadgeImage}
        currentPage={page}
        showOnPage={13}
        key={13}
        imageAlt="Merit Badge"
      >
        <Question>Can we ourselves make this satisfaction?</Question>
        <Answer>
          Certainly not; on the contrary, we daily increase our guilt.
          <BiblePopover
            refs={[
              { b: 'Job', c: 9, vf: 2, vt: 3 },
              { b: 'Job', c: 15, vf: 15, vt: 16 },
              { b: 'Matthew', c: 6, vf: 12 },
              { b: 'Matthew', c: 16, vf: 26 },
            ]}
          />
        </Answer>
        <Commentary f="004" q="013" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={sacrificeImage}
        currentPage={page}
        showOnPage={14}
        key={14}
        imageAlt="Sacrifice"
      >
        <Question>Can any mere creature make satisfaction for us?</Question>
        <Answer>
          None; for first, God will not punish any other creature for the sin
          which man committed;
          <BiblePopover refs={[{ b: 'Hebrews', c: 2, vf: 14, vt: 18 }]} />
          and further, no mere creature can sustain the burden of God’s eternal
          wrath against sin
          <BiblePopover refs={[{ b: 'Psalms', c: 130, vf: 3 }]} />
          and redeem others from it.
        </Answer>
        <Commentary f="004" q="014" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={twoNaturesImage}
        currentPage={page}
        showOnPage={15}
        key={15}
        imageAlt="True Man &amp; True God"
      >
        <Question>
          What kind of mediator and redeemer, then, must we seek?
        </Question>
        <Answer>
          One who is a true
          <BiblePopover
            refs={[
              { b: '1 Corinthians', c: 15, vf: 21, vt: 22 },
              { b: '1 Corinthians', c: 15, vf: 25, vt: 26 },
            ]}
          />
          and righteous man,
          <BiblePopover
            refs={[
              { b: 'Jeremiah', c: 13, vf: 16 },
              { b: 'Isaiah', c: 53, vf: 11 },
              { b: '2 Corinthians', c: 5, vf: 21 },
              { b: 'Hebrews', c: 7, vf: 15, vt: 16 },
            ]}
          />
          and yet more powerful than all creatures, that is, one who is also
          true God.
          <BiblePopover
            refs={[
              { b: 'Isaiah', c: 7, vf: 14 },
              { b: 'Hebrews', c: 7, vf: 26 },
            ]}
          />
        </Answer>
        <Commentary f="004" q="015" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={spotlessLambImage}
        currentPage={page}
        showOnPage={16}
        key={16}
        imageAlt="Spotless Lamb"
      >
        <LordsDay day={6} />
        <Question>Why must He be a true and righteous man?</Question>
        <Answer>
          Because the justice of God requires
          <BiblePopover refs={[{ b: 'Romans', c: 5, vf: 15 }]} />
          that the same human nature which has sinned should make satisfaction
          for sin; but one who is himself a sinner cannot satisfy for others.
          <BiblePopover refs={[{ b: 'Isaiah', c: 53, vf: 3, vt: 5 }]} />
        </Answer>
        <Commentary f="004" q="016" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={bearingWrathImage}
        currentPage={page}
        showOnPage={17}
        key={17}
        imageAlt="Bearing God’s Wrath"
      >
        <Question>Why must He also be true God?</Question>
        <Answer>
          That by the power of His Godhead He might bear in His manhood the
          burden of God’s wrath,
          <BiblePopover
            refs={[
              { b: 'Isaiah', c: 53, vf: 8 },
              { b: 'Acts', c: 2, vf: 24 },
            ]}
          />
          and so obtain for
          <BiblePopover
            refs={[
              { b: 'John', c: 3, vf: 16 },
              { b: 'Acts', c: 20, vf: 28 },
            ]}
          />
          and restore to us righteousness and life.
          <BiblePopover refs={[{ b: '1 John', c: 1, vf: 2 }]} />
        </Answer>
        <Commentary f="004" q="017" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={redemptionImage}
        currentPage={page}
        showOnPage={18}
        key={18}
        imageAlt="Complete Redemption"
      >
        <Question>
          But who now is that Mediator, who in one person is true God and also a
          true and righteous man?
        </Question>
        <Answer>
          Our Lord Jesus Christ,
          <BiblePopover
            refs={[
              { b: 'Matthew', c: 1, vf: 23 },
              { b: '1 Timothy', c: 3, vf: 16 },
              { b: 'Luke', c: 2, vf: 11 },
            ]}
          />
          who is freely given unto us for complete redemption and righteousness.
          <BiblePopover
            refs={[
              { b: '1 Corinthians', c: 1, vf: 30 },
              { b: 'Acts', c: 4, vf: 12 },
            ]}
          />
        </Answer>
        <Commentary f="004" q="018" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={goodNewsImage}
        currentPage={page}
        showOnPage={19}
        key={19}
        imageAlt="Good News"
      >
        <Question>From where do you know this?</Question>
        <Answer>
          From the Holy Gospel, which God Himself first revealed in Paradise,
          <BiblePopover refs={[{ b: 'Genesis', c: 3, vf: 15 }]} />
          afterwards proclaimed by the holy patriarchs
          <BiblePopover
            refs={[
              { b: 'Genesis', c: 22, vf: 18 },
              { b: 'Genesis', c: 49, vf: 10, vt: 11 },
              { b: 'Romans', c: 1, vf: 2 },
              { b: 'Hebrews', c: 1, vf: 1 },
              { b: 'Acts', c: 3, vf: 22, vt: 24 },
              { b: 'Acts', c: 10, vf: 43 },
            ]}
          />
          and prophets, and foreshadowed by the sacrifices and other ceremonies
          of the law,
          <BiblePopover
            refs={[
              { b: 'John', c: 5, vf: 46 },
              { b: 'Hebrews', c: 10, vf: 7 },
            ]}
          />
          and finally fulfilled by His well-beloved Son.
          <BiblePopover
            refs={[
              { b: 'Romans', c: 10, vf: 4 },
              { b: 'Hebrews', c: 10, vf: 1 },
            ]}
          />
        </Answer>
        <Commentary f="004" q="019" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={oliveTreeImage}
        currentPage={page}
        showOnPage={20}
        key={20}
        imageAlt="By Faith Engrafted"
      >
        <LordsDay day={7} />
        <Question>
          Are all men, then, saved by Christ as they have perished in Adam?
        </Question>
        <Answer>
          No, only those who by true faith are engrafted into Him and receive
          all His benefits.
          <BiblePopover
            refs={[
              { b: 'John', c: 1, vf: 12, vt: 13 },
              { b: '1 Corinthians', c: 15, vf: 22 },
              { b: 'Psalms', c: 2, vf: 12 },
              { b: 'Romans', c: 11, vf: 20 },
              { b: 'Hebrews', c: 4, vf: 2, vt: 3 },
              { b: 'Hebrews', c: 10, vf: 39 },
            ]}
          />
        </Answer>
        <Commentary f="004" q="020" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={trustImage}
        currentPage={page}
        showOnPage={21}
        key={21}
        imageAlt="Hearty Trust"
      >
        <Question>What is true faith?</Question>
        <Answer>
          True faith is not only a sure knowledge whereby I hold for truth all
          that God has revealed to us in His Word,
          <BiblePopover refs={[{ b: 'James', c: 1, vf: 6 }]} />
          but also a hearty trust,
          <BiblePopover
            refs={[
              { b: 'Romans', c: 4, vf: 16, vt: 18 },
              { b: 'Romans', c: 5, vf: 1 },
            ]}
          />
          which the Holy Spirit
          <BiblePopover
            refs={[
              { b: '2 Corinthians', c: 4, vf: 13 },
              { b: 'Philippians', c: 1, vf: 19 },
              { b: 'Philippians', c: 1, vf: 29 },
            ]}
          />
          works in me by the Gospel,
          <BiblePopover
            refs={[
              { b: 'Romans', c: 1, vf: 16 },
              { b: 'Romans', c: 10, vf: 17 },
            ]}
          />
          that not only to others, but to me also, forgiveness of sins,
          everlasting righteousness, and salvation are freely given by God,
          <BiblePopover
            refs={[
              { b: 'Hebrews', c: 11, vf: 1, vt: 2 },
              { b: 'Romans', c: 1, vf: 17 },
            ]}
          />
          merely of grace, only for the sake of Christ’s merits.
          <BiblePopover
            refs={[
              { b: 'Ephesians', c: 2, vf: 7, vt: 9 },
              { b: 'Romans', c: 3, vf: 24, vt: 25 },
              { b: 'Galatians', c: 2, vf: 16 },
              { b: 'Acts', c: 10, vf: 43 },
            ]}
          />
        </Answer>
        <Commentary f="004" q="021" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={athanasiusImage}
        currentPage={page}
        showOnPage={22}
        key={22}
        imageAlt="Athanasius of Alexandria"
      >
        <Question>
          What, then, is necessary for a Christian to believe?
        </Question>
        <Answer>
          All that is promised us in the Gospel,
          <BiblePopover
            refs={[
              { b: 'John', c: 20, vf: 31 },
              { b: 'Matthew', c: 28, vf: 20 },
              { b: '2 Peter', c: 1, vf: 21 },
              { b: '2 Timothy', c: 3, vf: 15 },
            ]}
          />
          which the articles of our catholic, undoubted Christian faith teach us
          in summary.
        </Answer>
        <Commentary f="004" q="022" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={apostlesCreedImage}
        currentPage={page}
        showOnPage={23}
        key={23}
        imageAlt="Apostle’s Creed"
      >
        <Question>What are these articles?</Question>
        <Answer>
          I believe in God the Father Almighty, Maker of heaven and earth.
          <br />
          <br />
          And in Jesus Christ, His only-begotten Son, our Lord: who was
          conceived by the Holy Spirit, born of the virgin Mary, suffered under
          Pontius Pilate, was crucified, dead, and buried; He descended into
          hell; the third day He rose from the dead; He ascended into heaven,
          and sits at the right hand of God the Father Almighty; From there He
          will come to judge the living and the dead.
          <br />
          <br />I believe in the Holy Spirit, the holy, catholic Church, the
          communion of saints, the forgiveness of sins, the resurrection of the
          body, and the life everlasting.
        </Answer>
        <Commentary f="004" q="023" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={creationImage}
        currentPage={page}
        showOnPage={24}
        key={24}
        imageAlt="Creation"
      >
        <LordsDay day={8} />
        <Question>How are these articles divided?</Question>
        <Answer>
          Into three parts: the first is of God the Father and our creation; the
          second, of God the Son and our redemption; the third, of God the Holy
          Spirit and our sanctification.
          <BiblePopover
            refs={[
              { b: '1 Peter', c: 1, vf: 2 },
              { b: '1 John', c: 5, vf: 7 },
            ]}
          />
        </Answer>
        <Commentary f="004" q="024" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={trinityImage}
        currentPage={page}
        showOnPage={25}
        key={25}
        imageAlt="The Trinity"
      >
        <Question>
          Since there is but one Divine Being,
          <BiblePopover refs={[{ b: 'Deuteronomy', c: 6, vf: 4 }]} />
          why do you speak of three persons: Father, Son, and Holy Spirit?
        </Question>
        <Answer>
          Because God has so revealed Himself in His Word,
          <BiblePopover
            refs={[
              { b: 'Isaiah', c: 61, vf: 1 },
              { b: 'Psalms', c: 110, vf: 1 },
              { b: 'Matthew', c: 3, vf: 16, vt: 17 },
              { b: 'Matthew', c: 28, vf: 19 },
              { b: '1 John', c: 5, vf: 7 },
              { b: '2 Corinthians', c: 13, vf: 14 },
            ]}
          />
          that these three distinct persons are the one, true, eternal God.
        </Answer>
        <Commentary f="004" q="025" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={birdsImage}
        currentPage={page}
        showOnPage={26}
        key={26}
        imageAlt="Yet your heavenly Father feedeth them"
      >
        <LordsDay day={9} />
        <Question>
          What do you believe when you say, “I believe in God the Father
          Almighty, Maker of heaven and earth”?
        </Question>
        <Answer>
          That the eternal Father of our Lord Jesus Christ, who of nothing made
          heaven and earth with all that is in them,
          <BiblePopover
            refs={[
              { b: 'Genesis', c: 1, vf: 31 },
              { b: 'Psalms', c: 33, vf: 6 },
              { b: 'Colossians', c: 1, vf: 16 },
              { b: 'Hebrews', c: 11, vf: 3 },
            ]}
          />
          who likewise upholds, and governs them by His eternal counsel and
          providence,
          <BiblePopover
            refs={[
              { b: 'Psalms', c: 104, vf: 2, vt: 5 },
              { b: 'Matthew', c: 10, vf: 30 },
              { b: 'Hebrews', c: 1, vf: 3 },
              { b: 'Psalms', c: 115, vf: 3 },
              { b: 'Acts', c: 17, vf: 24, vt: 25 },
            ]}
          />
          is for the sake of Christ, His Son, my God and my Father,
          <BiblePopover
            refs={[
              { b: 'John', c: 1, vf: 12 },
              { b: 'Romans', c: 8, vf: 15 },
              { b: 'Galatians', c: 4, vf: 5, vt: 7 },
              { b: 'Ephesians', c: 1, vf: 5 },
              { b: 'Ephesians', c: 3, vf: 14, vt: 16 },
              { b: 'Matthew', c: 6, vf: 8 },
            ]}
          />
          in whom I so trust as to have no doubt that He will provide me with
          all things necessary for body and soul;
          <BiblePopover
            refs={[
              { b: 'Psalms', c: 55, vf: 22 },
              { b: 'Matthew', c: 6, vf: 25, vt: 26 },
              { b: 'Luke', c: 12, vf: 22, vt: 24 },
              { b: 'Psalms', c: 90, vf: 1, vt: 2 },
            ]}
          />
          and further, that whatever evil He sends upon me in this valley of
          tears, He will turn to my good;
          <BiblePopover
            refs={[
              { b: 'Romans', c: 8, vf: 28 },
              { b: 'Acts', c: 17, vf: 27, vt: 28 },
            ]}
          />
          for He is able to do it, being Almighty God,
          <BiblePopover refs={[{ b: 'Romans', c: 10, vf: 12 }]} />
          and willing also, being a faithful Father.
          <BiblePopover
            refs={[
              { b: 'Matthew', c: 7, vf: 9, vt: 11 },
              { b: 'Numbers', c: 23, vf: 19 },
            ]}
          />
        </Answer>
        <Commentary f="004" q="026" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={bountyAndFamineImage}
        currentPage={page}
        showOnPage={27}
        key={27}
        imageAlt="Fruitful and Barren Years"
      >
        <LordsDay day={10} />
        <Question>What do you understand by the providence of God?</Question>
        <Answer>
          The almighty, everywhere-present power of God,
          <BiblePopover refs={[{ b: 'Acts', c: 17, vf: 25, vt: 26 }]} />
          whereby, as it were by His hand, He still upholds heaven and earth
          with all creatures,
          <BiblePopover refs={[{ b: 'Hebrews', c: 1, vf: 3 }]} />
          and so governs them that herbs and grass, rain and drought, fruitful
          and barren years, meat and drink,
          <BiblePopover
            refs={[
              { b: 'Jeremiah', c: 5, vf: 24 },
              { b: 'Acts', c: 14, vf: 17 },
            ]}
          />
          health and sickness,
          <BiblePopover refs={[{ b: 'John', c: 9, vf: 3 }]} />
          riches and poverty,
          <BiblePopover
            refs={[
              { b: 'Proverbs', c: 22, vf: 2 },
              { b: 'Psalms', c: 103, vf: 19 },
              { b: 'Romans', c: 5, vf: 3, vt: 5 },
            ]}
          />
          indeed, all things come not by chance, but by His fatherly hand.
        </Answer>
        <Commentary f="005" q="027" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={thankfulInProsperityImage}
        currentPage={page}
        showOnPage={28}
        key={28}
        imageAlt="Thankful in Prosperity"
      >
        <Question>
          What does it profit us to know that God created, and by His providence
          upholds, all things?
        </Question>
        <Answer>
          That we may be patient in adversity,
          <BiblePopover
            refs={[
              { b: 'Romans', c: 5, vf: 3 },
              { b: 'James', c: 1, vf: 3 },
              { b: 'Job', c: 1, vf: 21 },
            ]}
          />
          thankful in prosperity,
          <BiblePopover
            refs={[
              { b: 'Deuteronomy', c: 8, vf: 10 },
              { b: '1 Thessalonians', c: 5, vf: 18 },
            ]}
          />
          and for what is future have good confidence in our faithful God and
          Father, that no creature shall separate us from His love,
          <BiblePopover
            refs={[
              { b: 'Romans', c: 8, vf: 35 },
              { b: 'Romans', c: 8, vf: 38, vt: 39 },
            ]}
          />
          since all creatures are so in His hand, that without His will they
          cannot so much as move.
          <BiblePopover
            refs={[
              { b: 'Job', c: 1, vf: 12 },
              { b: 'Acts', c: 17, vf: 25, vt: 28 },
              { b: 'Proverbs', c: 21, vf: 1 },
              { b: 'Psalms', c: 71, vf: 7 },
              { b: '2 Corinthians', c: 1, vf: 10 },
            ]}
          />
        </Answer>
        <Commentary f="005" q="028" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={josephsDreamImage}
        currentPage={page}
        showOnPage={29}
        key={29}
        imageAlt="Joseph’s Dream (Matt. 1:21)"
      >
        <LordsDay day={11} />
        <Question>
          Why is the Son of God called “Jesus,” that is, Savior?
        </Question>
        <Answer>
          Because He saves us from all our sins,
          <BiblePopover
            refs={[
              { b: 'Matthew', c: 1, vf: 21 },
              { b: 'Hebrews', c: 7, vf: 25 },
            ]}
          />
          and because salvation is not to be sought or found in any other.
          <BiblePopover
            refs={[
              { b: 'Acts', c: 4, vf: 12 },
              { b: 'Luke', c: 2, vf: 10, vt: 11 },
            ]}
          />
        </Answer>
        <Commentary f="005" q="029" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={christPlusImage}
        currentPage={page}
        showOnPage={30}
        key={30}
        imageAlt="Christ Plus"
      >
        <Question>
          Do those also believe in the only Savior Jesus, who seek their
          salvation and welfare from “saints,” themselves, or anywhere else?
        </Question>
        <Answer>
          No; although they make their boast of Him, yet in their deeds they
          deny the only Savior Jesus;
          <BiblePopover
            refs={[
              { b: '1 Corinthians', c: 1, vf: 13 },
              { b: '1 Corinthians', c: 1, vf: 30, vt: 31 },
              { b: 'Galatians', c: 5, vf: 4 },
            ]}
          />
          for either Jesus is not a complete Savior, or they who by true faith
          receive this Savior, must have in Him all that is necessary to their
          salvation.
          <BiblePopover
            refs={[
              { b: 'Isaiah', c: 9, vf: 7 },
              { b: 'Colossians', c: 1, vf: 20 },
              { b: 'Colossians', c: 2, vf: 10 },
              { b: 'John', c: 1, vf: 16 },
              { b: 'Matthew', c: 23, vf: 28 },
            ]}
          />
        </Answer>
        <Commentary f="005" q="030" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={anointedImage}
        currentPage={page}
        showOnPage={31}
        key={31}
        imageAlt="Anointed"
      >
        <LordsDay day={12} />
        <Question>Why is He called “Christ,” that is, Anointed?</Question>
        <Answer>
          Because He is ordained of God the Father and anointed with the Holy
          Spirit
          <BiblePopover refs={[{ b: 'Hebrews', c: 1, vf: 9 }]} />
          to be our chief Prophet and Teacher,
          <BiblePopover
            refs={[
              { b: 'Deuteronomy', c: 18, vf: 15 },
              { b: 'Acts', c: 3, vf: 22 },
            ]}
          />
          who has fully revealed to us the secret counsel and will of God
          concerning our redemption;
          <BiblePopover
            refs={[
              { b: 'John', c: 1, vf: 18 },
              { b: 'John', c: 15, vf: 15 },
            ]}
          />
          and our only High Priest,
          <BiblePopover
            refs={[
              { b: 'Psalms', c: 110, vf: 4 },
              { b: 'Hebrews', c: 7, vf: 21 },
            ]}
          />
          who by the one sacrifice of His body, has redeemed us, and ever lives
          to make intercession for us with the Father;
          <BiblePopover refs={[{ b: 'Romans', c: 5, vf: 9, vt: 10 }]} />
          and our eternal King, who governs us by His Word and Spirit, and
          defends and preserves us in the redemption obtained for us.
          <BiblePopover
            refs={[
              { b: 'Psalms', c: 2, vf: 6 },
              { b: 'Luke', c: 1, vf: 33 },
              { b: 'Matthew', c: 28, vf: 18 },
              { b: 'Isaiah', c: 61, vf: 1, vt: 2 },
              { b: '1 Peter', c: 2, vf: 24 },
              { b: 'Revelation', c: 19, vf: 16 },
            ]}
          />
        </Answer>
        <Commentary f="005" q="031" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={professImage}
        currentPage={page}
        showOnPage={32}
        key={32}
        imageAlt="Confessing Christ"
      >
        <Question>But why are you called a Christian?</Question>
        <Answer>
          Because by faith I am a member of Christ
          <BiblePopover
            refs={[
              { b: 'Acts', c: 11, vf: 26 },
              { b: '1 John', c: 2, vf: 20 },
              { b: '1 John', c: 2, vf: 27 },
            ]}
          />
          and thus a partaker of His anointing,
          <BiblePopover refs={[{ b: 'Acts', c: 2, vf: 17 }]} />
          in order that I also may confess His Name,
          <BiblePopover refs={[{ b: 'Mark', c: 8, vf: 38 }]} />
          may present myself a living sacrifice of thankfulness to Him,
          <BiblePopover
            refs={[
              { b: 'Romans', c: 12, vf: 1 },
              { b: 'Revelation', c: 5, vf: 8 },
              { b: 'Revelation', c: 5, vf: 10 },
              { b: '1 Peter', c: 2, vf: 9 },
              { b: 'Revelation', c: 1, vf: 6 },
            ]}
          />
          and with a free conscience may fight against sin and the devil in this
          life,
          <BiblePopover refs={[{ b: '1 Timothy', c: 1, vf: 18, vt: 19 }]} />
          and hereafter in eternity reign with Him over all creatures.
          <BiblePopover
            refs={[
              { b: '2 Timothy', c: 2, vf: 12 },
              { b: 'Ephesians', c: 6, vf: 12 },
              { b: 'Revelation', c: 3, vf: 21 },
            ]}
          />
        </Answer>
        <Commentary f="005" q="032" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={adoptedImage}
        currentPage={page}
        showOnPage={33}
        key={33}
        imageAlt="Adopted"
      >
        <LordsDay day={13} />
        <Question>
          Why is He called God’s “only begotten Son,” since we also are the
          children of God?
        </Question>
        <Answer>
          Because Christ alone is the eternal, natural Son of God,
          <BiblePopover
            refs={[
              { b: 'John', c: 1, vf: 14 },
              { b: 'John', c: 1, vf: 18 },
            ]}
          />
          but we are children of God by adoption, through grace, for His sake.
          <BiblePopover
            refs={[
              { b: 'Romans', c: 8, vf: 15, vt: 17 },
              { b: 'Ephesians', c: 1, vf: 5, vt: 6 },
              { b: '1 John', c: 3, vf: 1 },
            ]}
          />
        </Answer>
        <Commentary f="005" q="033" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={crownOfThornsImage}
        currentPage={page}
        showOnPage={34}
        key={34}
        imageAlt="Crown of Thorns"
      >
        <Question>Why do you call Him “our Lord”?</Question>
        <Answer>
          Because not with silver or gold, but with His precious blood, He has
          redeemed and purchased us, body and soul, from sin and from all the
          power of the devil, to be His own.
          <BiblePopover
            refs={[
              { b: '1 Peter', c: 1, vf: 18, vt: 19 },
              { b: '1 Peter', c: 2, vf: 9 },
              { b: '1 Corinthians', c: 6, vf: 20 },
              { b: '1 Corinthians', c: 7, vf: 23 },
              { b: 'Acts', c: 2, vf: 36 },
              { b: 'Titus', c: 2, vf: 14 },
              { b: 'Colossians', c: 1, vf: 14 },
            ]}
          />
        </Answer>
        <Commentary f="006" q="034" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={maryAndJosephImage}
        currentPage={page}
        showOnPage={35}
        key={35}
        imageAlt="Mary and Joseph"
      >
        <LordsDay day={14} />
        <Question>
          What is the meaning of “conceived by the Holy Spirit, born of the
          virgin Mary?”
        </Question>
        <Answer>
          That the eternal Son of God, who is
          <BiblePopover
            refs={[
              { b: 'John', c: 1, vf: 1 },
              { b: 'Romans', c: 1, vf: 3, vt: 4 },
            ]}
          />
          and continues true and eternal God,
          <BiblePopover refs={[{ b: 'Romans', c: 9, vf: 5 }]} />
          took upon Himself the very nature of man, of the flesh and blood of
          the virgin Mary,
          <BiblePopover
            refs={[
              { b: 'Galatians', c: 4, vf: 4 },
              { b: 'John', c: 1, vf: 14 },
            ]}
          />
          by the operation of the Holy Spirit;
          <BiblePopover
            refs={[
              { b: 'Matthew', c: 1, vf: 18, vt: 20 },
              { b: 'Luke', c: 1, vf: 35 },
            ]}
          />
          so that He might also be the true seed of David,
          <BiblePopover refs={[{ b: 'Psalms', c: 132, vf: 11 }]} />
          like unto His brethren in all things,
          <BiblePopover refs={[{ b: 'Philippians', c: 2, vf: 7 }]} />
          except for sin.
          <BiblePopover
            refs={[
              { b: 'Hebrews', c: 4, vf: 15 },
              { b: '1 John', c: 5, vf: 20 },
            ]}
          />
        </Answer>
        <Commentary f="006" q="035" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={clothedInRighteousnessImage}
        currentPage={page}
        showOnPage={36}
        key={36}
        imageAlt="Clothed in Righteousness"
      >
        <Question>
          What benefit do you receive from the holy conception and birth of
          Christ?
        </Question>
        <Answer>
          That He is our Mediator,
          <BiblePopover refs={[{ b: 'Hebrews', c: 2, vf: 16, vt: 17 }]} />
          and with His innocence and perfect holiness covers, in the sight of
          God, my sin, wherein I was conceived.
          <BiblePopover
            refs={[
              { b: 'Psalms', c: 32, vf: 1 },
              { b: '1 John', c: 1, vf: 9 },
            ]}
          />
        </Answer>
        <Commentary f="006" q="036" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={crossAndHammerImage}
        currentPage={page}
        showOnPage={37}
        key={37}
        imageAlt="His Suffering"
      >
        <LordsDay day={15} />
        <Question>What do you understand by the word “suffered”?</Question>
        <Answer>
          That all the time He lived on earth, but especially at the end of His
          life, He bore, in body and soul, the wrath of God against the sin of
          the whole human race;
          <BiblePopover
            refs={[
              { b: '1 Peter', c: 2, vf: 24 },
              { b: 'Isaiah', c: 53, vf: 12 },
            ]}
          />
          in order that by His suffering, as the only atoning sacrifice,
          <BiblePopover
            refs={[
              { b: '1 John', c: 2, vf: 2 },
              { b: '1 John', c: 4, vf: 10 },
              { b: 'Romans', c: 3, vf: 25, vt: 26 },
              { b: 'Psalms', c: 22, vf: 14, vt: 16 },
              { b: 'Matthew', c: 26, vf: 38 },
              { b: 'Romans', c: 5, vf: 6 },
            ]}
          />
          He might redeem our body and soul from everlasting damnation, and
          obtain for us the grace of God, righteousness, and eternal life.
        </Answer>
        <Commentary f="006" q="037" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={pontiusPilotImage}
        currentPage={page}
        showOnPage={38}
        key={38}
        imageAlt="Pontius Pilate"
      >
        <Question>Why did He suffer “under Pontius Pilate” as judge?</Question>
        <Answer>
          That He, being innocent, might be condemned by the temporal judge,
          <BiblePopover
            refs={[
              { b: 'Acts', c: 4, vf: 27, vt: 28 },
              { b: 'Luke', c: 23, vf: 13, vt: 15 },
              { b: 'John', c: 19, vf: 4 },
            ]}
          />
          and thereby deliver us from the severe judgment of God, to which we
          were exposed.
          <BiblePopover
            refs={[
              { b: 'Psalms', c: 69, vf: 4 },
              { b: '2 Corinthians', c: 5, vf: 21 },
              { b: 'Matthew', c: 27, vf: 24 },
            ]}
          />
        </Answer>
        <Commentary f="006" q="038" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={accursedOfGodImage}
        currentPage={page}
        showOnPage={39}
        key={39}
        imageAlt="Accursed of God"
      >
        <Question>
          Is there anything more in His having been “crucified” than if He had
          suffered some other death?
        </Question>
        <Answer>
          Yes, for thereby I am assured that He took upon Himself the curse
          which lay upon me,
          <BiblePopover refs={[{ b: 'Galatians', c: 3, vf: 13, vt: 14 }]} />
          because the death of the cross was accursed of God.
          <BiblePopover
            refs={[
              { b: 'Deuteronomy', c: 21, vf: 22, vt: 23 },
              { b: 'Philippians', c: 2, vf: 8 },
            ]}
          />
        </Answer>
        <Commentary f="006" q="039" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={withASpearImage}
        currentPage={page}
        showOnPage={40}
        key={40}
        imageAlt="With a Spear Pierced His Side"
      >
        <LordsDay day={16} />
        <Question>Why was it necessary for Christ to suffer “death”?</Question>
        <Answer>
          Because the justice and truth
          <BiblePopover refs={[{ b: 'Genesis', c: 2, vf: 17 }]} />
          of God required that satisfaction for our sins could be made in no
          other way than by the death of the Son of God.
          <BiblePopover
            refs={[
              { b: 'Hebrews', c: 2, vf: 9 },
              { b: 'Romans', c: 6, vf: 23 },
            ]}
          />
        </Answer>
        <Commentary f="006" q="040" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={aloeAndMyrrhImage}
        currentPage={page}
        showOnPage={41}
        key={41}
        imageAlt="Mixture of Myrrh and Aloes"
      >
        <Question>Why was He “buried”?</Question>
        <Answer>
          To show thereby that He was really dead.
          <BiblePopover
            refs={[
              { b: 'Matthew', c: 27, vf: 59, vt: 60 },
              { b: 'John', c: 19, vf: 38, vt: 42 },
              { b: 'Acts', c: 13, vf: 29 },
            ]}
          />
        </Answer>
        <Commentary f="006" q="041" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={graveyardImage}
        currentPage={page}
        showOnPage={42}
        key={42}
        imageAlt="Our Death"
      >
        <Question>
          Since, then, Christ died for us, why must we also die?
        </Question>
        <Answer>
          Our death is not a satisfaction for our sin, but only a dying to sin
          and an entering into eternal life.
          <BiblePopover
            refs={[
              { b: 'John', c: 5, vf: 24 },
              { b: 'Philippians', c: 1, vf: 23 },
              { b: 'Romans', c: 7, vf: 24, vt: 25 },
            ]}
          />
        </Answer>
        <Commentary f="006" q="042" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={oldManImage}
        currentPage={page}
        showOnPage={43}
        key={43}
        imageAlt="The Old Man"
      >
        <Question>
          What further benefit do we receive from the sacrifice and death of
          Christ on the cross?
        </Question>
        <Answer>
          That by His power our old man is with Him crucified, slain, and
          buried;
          <BiblePopover
            refs={[
              { b: 'Romans', c: 6, vf: 6, vt: 8 },
              { b: 'Colossians', c: 2, vf: 12 },
            ]}
          />
          so that the evil lusts of the flesh may no more reign in us,
          <BiblePopover refs={[{ b: 'Romans', c: 6, vf: 12 }]} />
          but that we may offer ourselves unto Him a sacrifice of thanksgiving.
          <BiblePopover
            refs={[
              { b: 'Romans', c: 12, vf: 1 },
              { b: '2 Corinthians', c: 5, vf: 15 },
            ]}
          />
        </Answer>
        <Commentary f="006" q="043" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={flamesOfHellImage}
        currentPage={page}
        showOnPage={44}
        key={44}
        imageAlt="Torment of Hell"
      >
        <Question>Why is it added: “He descended into hell”?</Question>
        <Answer>
          That in my greatest temptations I may be assured that Christ my Lord,
          by His inexpressible anguish, pains, and terrors, which He suffered in
          His soul on the cross and before, has redeemed me from the anguish and
          torment of hell.
          <BiblePopover
            refs={[
              { b: 'Isaiah', c: 53, vf: 10 },
              { b: 'Matthew', c: 27, vf: 46 },
              { b: 'Psalms', c: 18, vf: 5 },
              { b: 'Psalms', c: 116, vf: 3 },
            ]}
          />
        </Answer>
        <Commentary f="007" q="044" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={graceImage}
        currentPage={page}
        showOnPage={45}
        key={45}
        imageAlt="The Empty Tomb"
      >
        <LordsDay day={17} />
        <Question>
          What benefit do we receive from the “resurrection” of Christ?
        </Question>
        <Answer>
          First, by His resurrection He has overcome death, that He might make
          us partakers of the righteousness which He has obtained for us by His
          death.
          <BiblePopover
            refs={[
              { b: '1 Corinthians', c: 15, vf: 15 },
              { b: '1 Corinthians', c: 15, vf: 17 },
              { b: '1 Corinthians', c: 15, vf: 54, vt: 55 },
              { b: 'Romans', c: 4, vf: 25 },
              { b: '1 Peter', c: 1, vf: 3, vt: 4 },
              { b: '1 Peter', c: 1, vf: 21 },
            ]}
          />
          Second, by His power we are also now raised up to a new life.
          <BiblePopover
            refs={[
              { b: 'Romans', c: 6, vf: 4 },
              { b: 'Colossians', c: 3, vf: 1, vt: 4 },
              { b: 'Ephesians', c: 2, vf: 5 },
            ]}
          />
          Third, the resurrection of Christ is to us a sure pledge of our
          blessed resurrection.
          <BiblePopover
            refs={[
              { b: '1 Corinthians', c: 15, vf: 12 },
              { b: '1 Corinthians', c: 15, vf: 20, vt: 21 },
              { b: 'Romans', c: 8, vf: 11 },
            ]}
          />
        </Answer>
        <Commentary f="007" q="045" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={ascensionImage}
        currentPage={page}
        showOnPage={46}
        key={46}
        imageAlt="The Ascension"
      >
        <Question>
          What do you understand by the words “He ascended into heaven”?
        </Question>
        <Answer>
          That Christ, in the sight of His disciples, was taken up from the
          earth into heaven,
          <BiblePopover
            refs={[
              { b: 'Acts', c: 1, vf: 9 },
              { b: 'Matthew', c: 26, vf: 64 },
              { b: 'Mark', c: 16, vf: 19 },
              { b: 'Luke', c: 24, vf: 51 },
            ]}
          />
          and continues there in our behalf
          <BiblePopover
            refs={[
              { b: 'Hebrews', c: 4, vf: 14 },
              { b: 'Hebrews', c: 7, vf: 24, vt: 25 },
              { b: 'Hebrews', c: 9, vf: 11 },
              { b: 'Romans', c: 8, vf: 34 },
              { b: 'Ephesians', c: 4, vf: 10 },
            ]}
          />
          until He shall come again to judge the living and the dead.
          <BiblePopover
            refs={[
              { b: 'Acts', c: 1, vf: 11 },
              { b: 'Acts', c: 3, vf: 20, vt: 21 },
              { b: 'Matthew', c: 24, vf: 30 },
            ]}
          />
        </Answer>
        <Commentary f="007" q="046" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={preachingImage}
        currentPage={page}
        showOnPage={47}
        key={47}
        imageAlt="Christ With His Church"
      >
        <Question>
          But is not Christ with us even unto the end of the world, as He has
          promised?
          <BiblePopover refs={[{ b: 'Matthew', c: 28, vf: 20 }]} />
        </Question>
        <Answer>
          Christ is true man and true God. According to His human nature He is
          now not on earth,
          <BiblePopover
            refs={[
              { b: 'Matthew', c: 26, vf: 11 },
              { b: 'John', c: 16, vf: 28 },
              { b: 'John', c: 17, vf: 11 },
            ]}
          />
          but according to His Godhead, majesty, grace, and Spirit, He is at no
          time absent from us.
          <BiblePopover
            refs={[
              { b: 'John', c: 14, vf: 17, vt: 18 },
              { b: 'John', c: 16, vf: 13 },
              { b: 'Ephesians', c: 4, vf: 8 },
              { b: 'Matthew', c: 18, vf: 20 },
              { b: 'Hebrews', c: 8, vf: 4 },
            ]}
          />
        </Answer>
        <Commentary f="007" q="047" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={twoNaturesImage}
        currentPage={page}
        showOnPage={48}
        key={48}
        imageAlt="Two Natures"
      >
        <Question>
          But are not, in this way, the two natures in Christ separated from one
          another, if the manhood is not wherever the Godhead is?
        </Question>
        <Answer>
          Not at all, for since the Godhead is incomprehensible and everywhere
          present,
          <BiblePopover
            refs={[
              { b: 'Acts', c: 7, vf: 49 },
              { b: 'Jeremiah', c: 23, vf: 24 },
            ]}
          />
          it must follow that the same is not limited with the human nature He
          assumed, and yet remains personally united to it.
          <BiblePopover
            refs={[
              { b: 'Colossians', c: 2, vf: 9 },
              { b: 'John', c: 1, vf: 48 },
              { b: 'John', c: 3, vf: 13 },
              { b: 'John', c: 11, vf: 15 },
              { b: 'Matthew', c: 28, vf: 6 },
            ]}
          />
        </Answer>
        <Commentary f="007" q="048" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={pentecostImage}
        currentPage={page}
        showOnPage={49}
        key={49}
        imageAlt="The Sending of the Holy Spirit"
      >
        <LordsDay day={18} />
        <Question>
          What benefit do we receive from Christ’s ascension into heaven?
        </Question>
        <Answer>
          First, that He is our Advocate in the presence of His Father in
          heaven.
          <BiblePopover
            refs={[
              { b: '1 John', c: 2, vf: 1 },
              { b: 'Romans', c: 8, vf: 34 },
            ]}
          />
          Second, that we have our flesh in heaven as a sure pledge, that He as
          the Head, will also take us, His members, up to Himself.
          <BiblePopover
            refs={[
              { b: 'John', c: 14, vf: 2 },
              { b: 'John', c: 20, vf: 17 },
              { b: 'Ephesians', c: 2, vf: 6 },
            ]}
          />
          Third, that He sends us His Spirit as an earnest,
          <BiblePopover
            refs={[
              { b: 'John', c: 14, vf: 16 },
              { b: 'Acts', c: 2, vf: 33 },
              { b: '2 Corinthians', c: 5, vf: 5 },
            ]}
          />
          by whose power we seek those things which are above, where Christ sits
          at the right hand of God, and not things on the earth.4
          <BiblePopover
            refs={[
              { b: 'Colossians', c: 3, vf: 1 },
              { b: 'John', c: 14, vf: 3 },
              { b: 'Hebrews', c: 9, vf: 24 },
            ]}
          />
        </Answer>
        <Commentary f="007" q="049" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={kingOfKingsImage}
        currentPage={page}
        showOnPage={50}
        key={50}
        imageAlt="Above All Principality and Power"
      >
        <Question>
          Why is it added: “And sits at the right hand of God”?
        </Question>
        <Answer>
          Because Christ ascended into heaven for this end, that He might there
          appear as the Head of His Church,
          <BiblePopover
            refs={[
              { b: 'Ephesians', c: 1, vf: 20, vt: 23 },
              { b: 'Colossians', c: 1, vf: 18 },
            ]}
          />
          by whom the Father governs all things.
          <BiblePopover
            refs={[
              { b: 'John', c: 5, vf: 22 },
              { b: '1 Peter', c: 3, vf: 22 },
              { b: 'Psalms', c: 110, vf: 1 },
            ]}
          />
        </Answer>
        <Commentary f="007" q="050" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={defendsTheChurchImage}
        currentPage={page}
        showOnPage={51}
        key={51}
        imageAlt="Defends and Preserves His Church"
      >
        <LordsDay day={19} />
        <Question>
          What does this glory of Christ, our Head, profit us?
        </Question>
        <Answer>
          First, that by His Holy Spirit He pours out heavenly gifts upon us,
          His members;
          <BiblePopover refs={[{ b: 'Ephesians', c: 4, vf: 10, vt: 12 }]} />
          then, that by His power He defends and preserves us against all
          enemies.
          <BiblePopover
            refs={[
              { b: 'Psalms', c: 2, vf: 9 },
              { b: 'John', c: 10, vf: 28, vt: 30 },
              { b: '1 Corinthians', c: 15, vf: 25, vt: 26 },
              { b: 'Acts', c: 2, vf: 33 },
            ]}
          />
        </Answer>
        <Commentary f="007" q="051" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={upliftedHeadImage}
        currentPage={page}
        showOnPage={52}
        key={52}
        imageAlt="With Uplifted Head"
      >
        <Question>
          What comfort is it to you that Christ “shall come to judge the living
          and the dead”?
        </Question>
        <Answer>
          That in all my sorrows and persecutions, I, with uplifted head, look
          for the very One who offered Himself for me to the judgment of God,
          and removed all curse from me, to come as Judge from heaven,
          <BiblePopover
            refs={[
              { b: 'Luke', c: 21, vf: 28 },
              { b: 'Romans', c: 8, vf: 23, vt: 24 },
              { b: 'Philippians', c: 3, vf: 20, vt: 21 },
              { b: 'Titus', c: 2, vf: 13 },
            ]}
          />
          who shall cast all His and my enemies into everlasting condemnation,
          <BiblePopover
            refs={[
              { b: '2 Thessalonians', c: 1, vf: 6 },
              { b: '2 Thessalonians', c: 1, vf: 10 },
              { b: '1 Thessalonians', c: 4, vf: 16, vt: 18 },
              { b: 'Matthew', c: 25, vf: 41 },
            ]}
          />
          but shall take me with all His chosen ones to Himself into heavenly
          joy and glory.
          <BiblePopover
            refs={[
              { b: 'Acts', c: 1, vf: 10, vt: 11 },
              { b: 'Hebrews', c: 9, vf: 28 },
            ]}
          />
        </Answer>
        <Commentary f="007" q="052" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={sealedImage}
        currentPage={page}
        showOnPage={53}
        key={53}
        imageAlt="Who hath also sealed us (2 Cor. 1:22)"
      >
        <LordsDay day={20} />
        <Question>What do you believe concerning the “Holy Spirit”?</Question>
        <Answer>
          First, that He is co-eternal God with the Father and the Son.
          <BiblePopover
            refs={[
              { b: 'Genesis', c: 1, vf: 2 },
              { b: 'Isaiah', c: 48, vf: 16 },
              { b: '1 Corinthians', c: 3, vf: 16 },
              { b: '1 Corinthians', c: 6, vf: 19 },
              { b: 'Acts', c: 5, vf: 3, vt: 4 },
            ]}
          />
          Second, that He is also given unto me:
          <BiblePopover
            refs={[
              { b: 'Matthew', c: 28, vf: 19 },
              { b: '2 Corinthians', c: 1, vf: 21, vt: 22 },
            ]}
          />
          by true faith makes me a partaker of Christ and all His benefits,
          <BiblePopover
            refs={[
              { b: '1 Peter', c: 1, vf: 2 },
              { b: '1 Corinthians', c: 6, vf: 17 },
            ]}
          />
          comforts me,
          <BiblePopover refs={[{ b: 'Acts', c: 9, vf: 31 }]} />
          and shall abide with me forever.
          <BiblePopover
            refs={[
              { b: 'John', c: 14, vf: 16 },
              { b: '1 Peter', c: 4, vf: 14 },
              { b: '1 John', c: 4, vf: 13 },
              { b: 'Romans', c: 15, vf: 13 },
            ]}
          />
        </Answer>
        <Commentary f="008" q="053" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={multiplyAsStarsImage}
        currentPage={page}
        showOnPage={54}
        key={54}
        imageAlt="I will make thy seed to multiply as the stars (Gen. 26:4)"
      >
        <LordsDay day={21} />
        <Question>
          What do you believe concerning the “holy, catholic Church”?
        </Question>
        <Answer>
          That out of the whole human race,
          <BiblePopover refs={[{ b: 'Genesis', c: 26, vf: 4 }]} />
          from the beginning to the end of the world,
          <BiblePopover refs={[{ b: 'John', c: 10, vf: 10 }]} />
          the Son of God,
          <BiblePopover refs={[{ b: 'Ephesians', c: 1, vf: 10, vt: 13 }]} />
          by His Spirit and Word,
          <BiblePopover
            refs={[
              { b: 'Romans', c: 1, vf: 16 },
              { b: 'Isaiah', c: 59, vf: 21 },
              { b: 'Romans', c: 10, vf: 14, vt: 17 },
              { b: 'Ephesians', c: 5, vf: 26 },
            ]}
          />
          gathers, defends, and preserves for Himself unto everlasting life a
          chosen communion
          <BiblePopover
            refs={[
              { b: 'Romans', c: 8, vf: 29, vt: 30 },
              { b: 'Matthew', c: 16, vf: 18 },
              { b: 'Ephesians', c: 4, vf: 3, vt: 6 },
            ]}
          />
          in the unity of the true faith;
          <BiblePopover
            refs={[
              { b: 'Acts', c: 2, vf: 46 },
              { b: 'Psalms', c: 71, vf: 18 },
              { b: '1 Corinthians', c: 11, vf: 26 },
              { b: 'John', c: 10, vf: 28, vt: 30 },
              { b: '1 Corinthians', c: 1, vf: 8, vt: 9 },
            ]}
          />
          and that I am and forever shall remain a living member of this
          communion.
          <BiblePopover
            refs={[
              { b: '1 John', c: 3, vf: 21 },
              { b: '1 John', c: 2, vf: 19 },
              { b: 'Galatians', c: 3, vf: 28 },
            ]}
          />
        </Answer>
        <Commentary f="008" q="054" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={communionOfSaintsImage}
        currentPage={page}
        showOnPage={55}
        key={55}
        imageAlt="Welfare of Other Members"
      >
        <Question>
          What do you understand by the “communion of saints”?
        </Question>
        <Answer>
          First, that believers, one and all, as members of the Lord Jesus
          Christ, are partakers with Him in all His treasures and gifts;
          <BiblePopover refs={[{ b: '1 John', c: 1, vf: 3 }]} />
          second, that each one must feel himself bound to use his gifts readily
          and cheerfully for the advantage and welfare of other members.
          <BiblePopover
            refs={[
              { b: '1 Corinthians', c: 12, vf: 12, vt: 13 },
              { b: '1 Corinthians', c: 12, vf: 21 },
              { b: '1 Corinthians', c: 13, vf: 5, vt: 6 },
              { b: 'Philippians', c: 2, vf: 4, vt: 6 },
              { b: 'Hebrews', c: 3, vf: 14 },
            ]}
          />
        </Answer>
        <Commentary f="008" q="055" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={paidInFullImage}
        currentPage={page}
        showOnPage={56}
        key={56}
        imageAlt="No More Remember My Sins"
      >
        <Question>
          What do you believe concerning the “forgiveness of sins”?
        </Question>
        <Answer>
          That God, for the sake of Christ’s satisfaction,
          <BiblePopover refs={[{ b: '1 John', c: 2, vf: 2 }]} />
          will no more remember my sins, nor the sinful nature with which I have
          to struggle all my life long;
          <BiblePopover
            refs={[
              { b: '2 Corinthians', c: 5, vf: 19 },
              { b: '2 Corinthians', c: 5, vf: 21 },
              { b: 'Romans', c: 7, vf: 24, vt: 25 },
              { b: 'Psalms', c: 103, vf: 3 },
              { b: 'Psalms', c: 103, vf: 10, vt: 12 },
              { b: 'Jeremiah', c: 31, vf: 34 },
              { b: 'Romans', c: 8, vf: 1, vt: 4 },
            ]}
          />
          but graciously imputes to me the righteousness of Christ, that I may
          nevermore come into condemnation.
          <BiblePopover
            refs={[
              { b: 'John', c: 3, vf: 18 },
              { b: 'Ephesians', c: 1, vf: 7 },
              { b: 'Romans', c: 4, vf: 7, vt: 8 },
              { b: 'Romans', c: 7, vf: 18 },
            ]}
          />
        </Answer>
        <Commentary f="009" q="056" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={resurrectedBodyImage}
        currentPage={page}
        showOnPage={57}
        key={57}
        imageAlt="The Resurrection of the Body"
      >
        <LordsDay day={22} />
        <Question>
          What comfort do you receive from the “resurrection of the body”?
        </Question>
        <Answer>
          That not only my soul after this life shall be immediately taken up to
          Christ its Head,
          <BiblePopover
            refs={[
              { b: 'Luke', c: 23, vf: 43 },
              { b: 'Philippians', c: 1, vf: 21, vt: 23 },
            ]}
          />
          but also that this my body, raised by the power of Christ, shall be
          reunited with my soul, and made like the glorious body of Christ.
          <BiblePopover
            refs={[
              { b: '1 Corinthians', c: 15, vf: 53, vt: 54 },
              { b: 'Job', c: 19, vf: 25, vt: 27 },
              { b: '1 John', c: 3, vf: 2 },
            ]}
          />
        </Answer>
        <Commentary f="009" q="057" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={eyeHasNotSeenImage}
        currentPage={page}
        showOnPage={58}
        key={58}
        imageAlt="Such As Eye Has Not Seen"
      >
        <Question>
          What comfort do you receive from the article “life everlasting”?
        </Question>
        <Answer>
          That, inasmuch as I now feel in my heart the beginning of eternal joy,
          <BiblePopover refs={[{ b: '2 Corinthians', c: 5, vf: 2, vt: 3 }]} />
          I shall after this life possess complete blessedness, such as eye has
          not seen, nor ear heard, neither has entered into the heart of man,
          <BiblePopover refs={[{ b: '1 Corinthians', c: 2, vf: 9 }]} />
          therein to praise God forever.
          <BiblePopover
            refs={[
              { b: 'John', c: 17, vf: 3 },
              { b: 'Romans', c: 8, vf: 23 },
              { b: '1 Peter', c: 1, vf: 8 },
            ]}
          />
        </Answer>
        <Commentary f="009" q="058" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={graceImage}
        currentPage={page}
        showOnPage={59}
        key={59}
        imageAlt="In Life"
      >
        <LordsDay day={23} />
        <Question>
          What does it help you now, that you believe all this?
        </Question>
        <Answer>
          That I am righteous in Christ before God, and an heir of eternal life.
          <BiblePopover
            refs={[
              { b: 'Habakkuk', c: 2, vf: 4 },
              { b: 'Romans', c: 1, vf: 17 },
              { b: 'John', c: 3, vf: 36 },
              { b: 'Titus', c: 3, vf: 7 },
              { b: 'Romans', c: 5, vf: 1 },
              { b: 'Romans', c: 8, vf: 16 },
            ]}
          />
        </Answer>
        <Commentary f="009" q="059" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={graceImage}
        currentPage={page}
        showOnPage={60}
        key={60}
        imageAlt="In Life"
      >
        <Question>How are you righteous before God?</Question>
        <Answer>
          Only by true faith in Jesus Christ:
          <BiblePopover
            refs={[
              { b: 'Romans', c: 3, vf: 21, vt: 25 },
              { b: 'Galatians', c: 2, vf: 16 },
              { b: 'Ephesians', c: 2, vf: 8, vt: 9 },
              { b: 'Philippians', c: 3, vf: 9 },
            ]}
          />
          that is, although my conscience accuses me, that I have grievously
          sinned against all the commandments of God, and have never kept any of
          them,
          <BiblePopover refs={[{ b: 'Romans', c: 3, vf: 9, vt: 10 }]} />
          and am still prone always to all evil;
          <BiblePopover refs={[{ b: 'Romans', c: 7, vf: 23 }]} />
          yet God, without any merit of mine,
          <BiblePopover refs={[{ b: 'Titus', c: 3, vf: 5 }]} />
          of mere grace,
          <BiblePopover
            refs={[
              { b: 'Romans', c: 3, vf: 24 },
              { b: 'Ephesians', c: 2, vf: 8 },
            ]}
          />
          grants and imputes to me the perfect satisfaction,
          <BiblePopover refs={[{ b: '1 John', c: 2, vf: 2 }]} />
          righteousness, and holiness of Christ,
          <BiblePopover
            refs={[
              { b: '1 John', c: 2, vf: 1 },
              { b: 'Romans', c: 4, vf: 4, vt: 5 },
              { b: '2 Corinthians', c: 5, vf: 19 },
            ]}
          />
          as if I had never committed nor had any sins, and had myself
          accomplished all the obedience which Christ has fulfilled for me;
          <BiblePopover refs={[{ b: '2 Corinthians', c: 5, vf: 21 }]} />
          if only I accept such benefit with a believing heart.
          <BiblePopover
            refs={[
              { b: 'John', c: 3, vf: 18 },
              { b: 'Romans', c: 3, vf: 28 },
              { b: 'Romans', c: 10, vf: 10 },
            ]}
          />
        </Answer>
        <Commentary f="009" q="060" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={graceImage}
        currentPage={page}
        showOnPage={61}
        key={61}
        imageAlt="In Life"
      >
        <Question>
          Why do you say that you are righteous by faith only?
        </Question>
        <Answer>
          Not that I am acceptable to God on account of the worthiness of my
          faith, but because only the satisfaction, righteousness, and holiness
          of Christ is my righteousness before God;
          <BiblePopover
            refs={[
              { b: '1 Corinthians', c: 1, vf: 30 },
              { b: '1 Corinthians', c: 2, vf: 2 },
            ]}
          />
          and I can receive the same and make it my own in no other way than by
          faith only.
          <BiblePopover
            refs={[
              { b: '1 John', c: 5, vf: 10 },
              { b: 'Isaiah', c: 53, vf: 5 },
              { b: 'Galatians', c: 3, vf: 22 },
              { b: 'Romans', c: 4, vf: 16 },
            ]}
          />
        </Answer>
        <Commentary f="009" q="061" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={graceImage}
        currentPage={page}
        showOnPage={62}
        key={62}
        imageAlt="In Life"
      >
        <LordsDay day={24} />
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="009" q="062" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={graceImage}
        currentPage={page}
        showOnPage={63}
        key={63}
        imageAlt="In Life"
      >
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="009" q="063" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={graceImage}
        currentPage={page}
        showOnPage={64}
        key={64}
        imageAlt="In Life"
      >
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="009" q="064" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={graceImage}
        currentPage={page}
        showOnPage={65}
        key={65}
        imageAlt="In Life"
      >
        <LordsDay day={25} />
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="009" q="065" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={graceImage}
        currentPage={page}
        showOnPage={66}
        key={66}
        imageAlt="In Life"
      >
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="009" q="066" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={graceImage}
        currentPage={page}
        showOnPage={67}
        key={67}
        imageAlt="In Life"
      >
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="009" q="067" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={graceImage}
        currentPage={page}
        showOnPage={68}
        key={68}
        imageAlt="In Life"
      >
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="009" q="068" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={graceImage}
        currentPage={page}
        showOnPage={69}
        key={69}
        imageAlt="In Life"
      >
        <LordsDay day={26} />
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="010" q="069" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={graceImage}
        currentPage={page}
        showOnPage={70}
        key={70}
        imageAlt="In Life"
      >
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="010" q="070" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={graceImage}
        currentPage={page}
        showOnPage={71}
        key={71}
        imageAlt="In Life"
      >
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="010" q="071" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={graceImage}
        currentPage={page}
        showOnPage={72}
        key={72}
        imageAlt="In Life"
      >
        <LordsDay day={27} />
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="010" q="072" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={graceImage}
        currentPage={page}
        showOnPage={73}
        key={73}
        imageAlt="In Life"
      >
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="010" q="073" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={graceImage}
        currentPage={page}
        showOnPage={74}
        key={74}
        imageAlt="In Life"
      >
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="010" q="074" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={graceImage}
        currentPage={page}
        showOnPage={75}
        key={75}
        imageAlt="In Life"
      >
        <LordsDay day={28} />
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="011" q="075" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={graceImage}
        currentPage={page}
        showOnPage={76}
        key={76}
        imageAlt="In Life"
      >
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="011" q="076" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={graceImage}
        currentPage={page}
        showOnPage={77}
        key={77}
        imageAlt="In Life"
      >
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="011" q="077" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={graceImage}
        currentPage={page}
        showOnPage={78}
        key={78}
        imageAlt="In Life"
      >
        <LordsDay day={29} />
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="011" q="078" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={graceImage}
        currentPage={page}
        showOnPage={79}
        key={79}
        imageAlt="In Life"
      >
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="011" q="079" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={graceImage}
        currentPage={page}
        showOnPage={80}
        key={80}
        imageAlt="In Life"
      >
        <LordsDay day={30} />
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="011" q="080" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={graceImage}
        currentPage={page}
        showOnPage={81}
        key={81}
        imageAlt="In Life"
      >
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="011" q="081" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={graceImage}
        currentPage={page}
        showOnPage={82}
        key={82}
        imageAlt="In Life"
      >
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="011" q="082" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={graceImage}
        currentPage={page}
        showOnPage={83}
        key={83}
        imageAlt="In Life"
      >
        <LordsDay day={31} />
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="012" q="083" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={graceImage}
        currentPage={page}
        showOnPage={84}
        key={84}
        imageAlt="In Life"
      >
        <Question>Question</Question>
        <Answer>Answer</Answer>
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={graceImage}
        currentPage={page}
        showOnPage={85}
        key={85}
        imageAlt="In Life"
      >
        <Question>Question</Question>
        <Answer>Answer</Answer>
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={graceImage}
        currentPage={page}
        showOnPage={86}
        key={86}
        imageAlt="In Life"
      >
        <LordsDay day={32} />
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="013" q="086" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={graceImage}
        currentPage={page}
        showOnPage={87}
        key={87}
        imageAlt="In Life"
      >
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="013" q="087" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={graceImage}
        currentPage={page}
        showOnPage={88}
        key={88}
        imageAlt="In Life"
      >
        <LordsDay day={33} />
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="013" q="088" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={graceImage}
        currentPage={page}
        showOnPage={89}
        key={89}
        imageAlt="In Life"
      >
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="013" q="089" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={graceImage}
        currentPage={page}
        showOnPage={90}
        key={90}
        imageAlt="In Life"
      >
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="013" q="090" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={graceImage}
        currentPage={page}
        showOnPage={91}
        key={91}
        imageAlt="In Life"
      >
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="013" q="091" />
      </QuestionAnswerGroup>
    </SliderLayout>
  )
}

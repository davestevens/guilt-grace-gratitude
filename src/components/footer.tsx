import React from 'react'
import { Link } from '@heroui/react'
import { CircleUser, Copyleft, CreativeCommons } from 'lucide-react'

export default function Footer() {
  return (
    <div className="flex w-full mt-auto">
      <div className="flex flex-col justify-center align-middle bg-slate-900 text-slate-400 w-full text-sm mt-4 min-h-[140px]">
        <div className="flex justify-center align-middle pl-4 pr-4">
          <div className="text-center max-w-[430px] grid gap-4">
            <p>
              <Link
                property="dct:title"
                className="text-sm"
                rel="cc:attributionURL"
                href="/about"
              >
                About GuiltGraceGratitude.org
              </Link>
            </p>
            <p>
              <Link
                property="dct:title"
                className="text-sm"
                rel="cc:attributionURL"
                href="https://guiltgracegratitude.org"
              >
                GuiltGraceGratitude.org
              </Link>{' '}
              by <span property="cc:attributionName">Dave Stevens</span> is
              licensed under{' '}
              <Link
                className="inline text-sm"
                href="https://creativecommons.org/licenses/by-sa/4.0/?ref=chooser-v1"
                target="_blank"
                rel="license noopener noreferrer"
              >
                Creative Commons Attribution-ShareAlike 4.0 International
                <CreativeCommons size={16} className="inline ml-2" />
                <CircleUser size={16} className="inline ml-1" />
                <Copyleft size={16} className="inline ml-1" />
              </Link>
            </p>
          </div>
        </div>
      </div>
    </div>
  )
}

#!/bin/bash
parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
cd "$parent_path"

echo "Compressing source files and nod_modules..."
# zip up dist and assets
tar -zcf dist.tar.gz ../../out > /dev/null 2>&1

echo "Copying compressed files to the server using SSH..."
TARGET_HOST=$1
USERID=$2
scp provision-server.sh "${USERID}@${TARGET_HOST}:/home/${USERID}"
scp ../nginx/sites-available/guiltgracegratitude.org.conf "${USERID}@${TARGET_HOST}:/home/${USERID}"
scp dist.tar.gz "${USERID}@${TARGET_HOST}:/home/${USERID}"
ssh -t "${USERID}@${TARGET_HOST}" './provision-server.sh'

rm dist.tar.gz

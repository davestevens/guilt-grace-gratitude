'use client'

import React from 'react'
import comfortInLifeImage from '../../../public/images/comfort_in_life.webp'
import comfortInDeathImage from '../../../public/images/comfort_in_death.webp'
import SliderLayout from '@/components/slider-layout'
import QuestionAnswerGroup from '@/components/question-answer-group'
import Question from '@/components/question'
import Answer from '@/components/answer'
import BiblePopover from '@/components/bible-popover'
import LordsDay from '@/components/lords-day'
import Commentary from '@/components/commentary'

export default function Comfort() {
  const minPage = 1
  const maxPage = 2
  const [page, setPage] = React.useState(minPage)

  return (
    <SliderLayout
      title="Comfort"
      minPage={minPage}
      maxPage={maxPage}
      page={page}
      setPage={setPage}
      nextSection="Part 1: Guilt"
      nextSectionMobile="1: Guilt"
      nextRoute="/guilt"
    >
      <QuestionAnswerGroup
        image={comfortInLifeImage}
        currentPage={page}
        showOnPage={1}
        key={1}
        imageAlt="In Life"
      >
        <LordsDay day={1} />
        <Question>What is your only comfort in life and in death?</Question>
        <Answer>
          That I, with body and soul, both in life and in death,
          <BiblePopover refs={[{ b: 'Romans', c: 14, vf: 7, vt: 8 }]} /> am not
          my own,
          <BiblePopover refs={[{ b: '1 Corinthians', c: 6, vf: 19 }]} /> but
          belong to my my faithful Savior Jesus Christ,
          <BiblePopover refs={[{ b: '1 Corinthians', c: 3, vf: 23 }]} /> who
          with His precious blood
          <BiblePopover refs={[{ b: '1 Peter', c: 1, vf: 18, vt: 19 }]} />
          has fully satisfied for all my sins,
          <BiblePopover
            refs={[
              { b: '1 John', c: 1, vf: 7 },
              { b: '1 John', c: 2, vf: 2 },
            ]}
          />{' '}
          and redeemed me from all the power of the devil;
          <BiblePopover refs={[{ b: '1 John', c: 3, vf: 8 }]} /> and so
          preserves me
          <BiblePopover refs={[{ b: 'John', c: 6, vf: 39 }]} /> that without the
          will of my Father in heaven not a hair can fall from my head;
          <BiblePopover
            refs={[
              { b: 'Matthew', c: 10, vf: 29, vt: 30 },
              { b: 'Luke', c: 21, vf: 18 },
            ]}
          />{' '}
          indeed, that all things must work together for my salvation.
          <BiblePopover refs={[{ b: 'Romans', c: 8, vf: 28 }]} /> Wherefore, by
          His Holy Spirit, He also assures me of eternal life,
          <BiblePopover
            refs={[
              { b: '2 Corinthians', c: 1, vf: 21, vt: 22 },
              { b: 'Ephesians', c: 1, vf: 13, vt: 14 },
              { b: 'Romans', c: 8, vf: 16 },
            ]}
          />{' '}
          and makes me heartily willing and ready from now on to live unto Him.
          <BiblePopover refs={[{ b: 'Romans', c: 8, vf: 1 }]} />
        </Answer>
        <Commentary f="003" q="001" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={comfortInDeathImage}
        currentPage={page}
        showOnPage={2}
        key={2}
        imageAlt="And In Death"
      >
        <Question>
          How many things are necessary for you to know, that in this comfort
          you may live and die happily?
        </Question>
        <Answer>
          Three things:
          <BiblePopover
            refs={[
              { b: 'Luke', c: 24, vf: 46, vt: 47 },
              { b: '1 Corinthians', c: 6, vf: 11 },
              { b: 'Titus', c: 3, vf: 3, vt: 7 },
            ]}
          />{' '}
          First, the greatness of my sin and misery.
          <BiblePopover
            refs={[
              { b: 'John', c: 9, vf: 41 },
              { b: 'John', c: 15, vf: 22 },
            ]}
          />{' '}
          Second, how I am redeemed from all my sins and misery.
          <BiblePopover refs={[{ b: 'John', c: 17, vf: 3 }]} /> Third, how I am
          to be thankful to God for such redemption.
          <BiblePopover
            refs={[
              { b: 'Ephesians', c: 5, vf: 8, vt: 11 },
              { b: '1 Peter', c: 2, vf: 9, vt: 12 },
              { b: 'Romans', c: 6, vf: 11, vt: 14 },
              { b: 'Romans', c: 7, vf: 24, vt: 25 },
              { b: 'Galatians', c: 3, vf: 13 },
              { b: 'Colossians', c: 3, vf: 17 },
            ]}
          />
        </Answer>
        <Commentary f="003" q="002" />
      </QuestionAnswerGroup>
    </SliderLayout>
  )
}

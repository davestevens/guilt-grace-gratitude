'use client'

import React from 'react'
import gratitudeImage from '../../../public/images/graditude.webp'
import SliderLayout from '@/components/slider-layout'
import QuestionAnswerGroup from '@/components/question-answer-group'
import Question from '@/components/question'
import Answer from '@/components/answer'
import Commentary from '@/components/commentary'
import LordsDay from '@/components/lords-day'

export default function Gratitude() {
  const minPage = 92
  const maxPage = 129
  const [page, setPage] = React.useState(minPage)

  return (
    <SliderLayout
      title="Part 3: Gratitude"
      minPage={minPage}
      maxPage={maxPage}
      page={page}
      setPage={setPage}
      prevSection="Part 2: Grace"
      prevSectionMobile="2: Grace"
      prevRoute="/grace"
    >
      <QuestionAnswerGroup
        image={gratitudeImage}
        currentPage={page}
        showOnPage={92}
        key={92}
        imageAlt="In Life"
      >
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="013" q="092" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={gratitudeImage}
        currentPage={page}
        showOnPage={93}
        key={93}
        imageAlt="In Life"
      >
        <LordsDay day={34} />
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="013" q="093" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={gratitudeImage}
        currentPage={page}
        showOnPage={94}
        key={94}
        imageAlt="In Life"
      >
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="013" q="094" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={gratitudeImage}
        currentPage={page}
        showOnPage={95}
        key={95}
        imageAlt="In Life"
      >
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="014" q="095" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={gratitudeImage}
        currentPage={page}
        showOnPage={96}
        key={96}
        imageAlt="In Life"
      >
        <LordsDay day={35} />
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="014" q="096" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={gratitudeImage}
        currentPage={page}
        showOnPage={97}
        key={97}
        imageAlt="In Life"
      >
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="014" q="097" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={gratitudeImage}
        currentPage={page}
        showOnPage={98}
        key={98}
        imageAlt="In Life"
      >
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="014" q="098" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={gratitudeImage}
        currentPage={page}
        showOnPage={99}
        key={99}
        imageAlt="In Life"
      >
        <LordsDay day={36} />
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="014" q="099" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={gratitudeImage}
        currentPage={page}
        showOnPage={100}
        key={100}
        imageAlt="In Life"
      >
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="014" q="100" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={gratitudeImage}
        currentPage={page}
        showOnPage={101}
        key={101}
        imageAlt="In Life"
      >
        <LordsDay day={37} />
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="014" q="101" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={gratitudeImage}
        currentPage={page}
        showOnPage={102}
        key={102}
        imageAlt="In Life"
      >
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="014" q="102" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={gratitudeImage}
        currentPage={page}
        showOnPage={103}
        key={103}
        imageAlt="In Life"
      >
        <LordsDay day={38} />
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="014" q="103" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={gratitudeImage}
        currentPage={page}
        showOnPage={104}
        key={104}
        imageAlt="In Life"
      >
        <LordsDay day={39} />
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="015" q="104" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={gratitudeImage}
        currentPage={page}
        showOnPage={105}
        key={105}
        imageAlt="In Life"
      >
        <LordsDay day={40} />
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="015" q="105" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={gratitudeImage}
        currentPage={page}
        showOnPage={106}
        key={106}
        imageAlt="In Life"
      >
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="015" q="106" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={gratitudeImage}
        currentPage={page}
        showOnPage={107}
        key={107}
        imageAlt="In Life"
      >
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="015" q="107" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={gratitudeImage}
        currentPage={page}
        showOnPage={108}
        key={108}
        imageAlt="In Life"
      >
        <LordsDay day={41} />
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="015" q="108" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={gratitudeImage}
        currentPage={page}
        showOnPage={109}
        key={109}
        imageAlt="In Life"
      >
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="015" q="109" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={gratitudeImage}
        currentPage={page}
        showOnPage={110}
        key={110}
        imageAlt="In Life"
      >
        <LordsDay day={42} />
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="015" q="110" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={gratitudeImage}
        currentPage={page}
        showOnPage={111}
        key={111}
        imageAlt="In Life"
      >
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="015" q="111" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={gratitudeImage}
        currentPage={page}
        showOnPage={112}
        key={112}
        imageAlt="In Life"
      >
        <LordsDay day={43} />
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="015" q="112" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={gratitudeImage}
        currentPage={page}
        showOnPage={113}
        key={113}
        imageAlt="In Life"
      >
        <LordsDay day={44} />
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="015" q="113" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={gratitudeImage}
        currentPage={page}
        showOnPage={114}
        key={114}
        imageAlt="In Life"
      >
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="015" q="114" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={gratitudeImage}
        currentPage={page}
        showOnPage={116}
        key={116}
        imageAlt="In Life"
      >
        <LordsDay day={45} />
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="016" q="116" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={gratitudeImage}
        currentPage={page}
        showOnPage={117}
        key={117}
        imageAlt="In Life"
      >
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="016" q="117" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={gratitudeImage}
        currentPage={page}
        showOnPage={118}
        key={118}
        imageAlt="In Life"
      >
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="016" q="118" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={gratitudeImage}
        currentPage={page}
        showOnPage={119}
        key={119}
        imageAlt="In Life"
      >
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="016" q="119" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={gratitudeImage}
        currentPage={page}
        showOnPage={120}
        key={120}
        imageAlt="In Life"
      >
        <LordsDay day={46} />
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="016" q="120" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={gratitudeImage}
        currentPage={page}
        showOnPage={121}
        key={121}
        imageAlt="In Life"
      >
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="016" q="121" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={gratitudeImage}
        currentPage={page}
        showOnPage={122}
        key={122}
        imageAlt="In Life"
      >
        <LordsDay day={47} />
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="016" q="122" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={gratitudeImage}
        currentPage={page}
        showOnPage={123}
        key={123}
        imageAlt="In Life"
      >
        <LordsDay day={48} />
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="016" q="123" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={gratitudeImage}
        currentPage={page}
        showOnPage={124}
        key={124}
        imageAlt="In Life"
      >
        <LordsDay day={49} />
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="016" q="124" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={gratitudeImage}
        currentPage={page}
        showOnPage={125}
        key={125}
        imageAlt="In Life"
      >
        <LordsDay day={50} />
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="016" q="125" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={gratitudeImage}
        currentPage={page}
        showOnPage={126}
        key={126}
        imageAlt="In Life"
      >
        <LordsDay day={51} />
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="016" q="126" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={gratitudeImage}
        currentPage={page}
        showOnPage={127}
        key={127}
        imageAlt="In Life"
      >
        <LordsDay day={52} />
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="016" q="127" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={gratitudeImage}
        currentPage={page}
        showOnPage={128}
        key={128}
        imageAlt="In Life"
      >
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="016" q="128" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={gratitudeImage}
        currentPage={page}
        showOnPage={129}
        key={129}
        imageAlt="In Life"
      >
        <Question>Question</Question>
        <Answer>Answer</Answer>
        <Commentary f="016" q="129" />
      </QuestionAnswerGroup>
    </SliderLayout>
  )
}

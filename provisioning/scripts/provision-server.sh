#!/bin/bash

echo "Starting provisioning script..."

# installing nginx
#if [ $(dpkg-query -W -f='${Status}' nginx 2>/dev/null | grep -c "ok installed") -eq 0 ];
#then
#    echo "Installing nginx..."
#    sudo apt install nginx
#fi

# Note: If you need to make changes to the nginx config, make the changes
#       manually using an ssh terminal and nano editor. Do this so you don't
#       have to re-apply the lets encrypt settings.
if ! [ -d /var/www/guiltgracegratitude.org ];
then
    echo "Creating /var/www/guiltgracegratitude.org directory..."
    sudo mkdir /var/www/guiltgracegratitude.org
    sudo cp ~/guiltgracegratitude.org.conf /etc/nginx/sites-available/
    sudo rm /etc/nginx/sites-enabled/default
    sudo ln -s /etc/nginx/sites-available/guiltgracegratitude.org.conf /etc/nginx/sites-enabled/guiltgracegratitude.org.conf
    sudo systemctl reload nginx
fi

# unzip dist and assets to www/guiltgracegratitude.org folder
sudo rm -rf /var/www/guiltgracegratitude.org/dist
sudo tar -xzf dist.tar.gz -C /var/www/guiltgracegratitude.org --no-same-owner
rm dist.tar.gz

# Only update nginx settings if you are resetting SSL certs also
# sudo cp ~/guiltgracegratitude.org.conf /etc/nginx/sites-available/
# sudo systemctl reload nginx

# Installing let encrypt SSL certificates
#if [ $(dpkg-query -W -f='${Status}' snapd 2>/dev/null | grep -c "ok installed") -eq 0 ];
#then
#    echo "Installing snapd..."
#    sudo apt install snapd
#fi

#if snap list | grep -q 'certbot';
#then
#    echo "cerbot already installed."
#else
#    # See systemctl list-timers to see scheduled renew job snap.certbot.renew.timer
#    echo "Installing certbot..."
#    sudo snap install --classic certbot
#    sudo ln -s /snap/bin/certbot /usr/bin/certbot
#    # enter domain as [beta|test].guiltgracegratitude.org (whatever lets encrypt can reach)
#    sudo certbot --nginx
#fi

# List existing certs with `sudo certbot certificates`

# Cleanup
rm guiltgracegratitude.org.conf
rm provision-server.sh

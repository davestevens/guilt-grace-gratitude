import React from 'react'
import BiblePassageService from '@/services/bible-passage-service'
import { Button, Popover, PopoverContent, PopoverTrigger } from '@heroui/react'
import { BookOpenText } from 'lucide-react'
import { Divider } from '@heroui/divider'
import { Link } from '@heroui/react'

interface BibleReference {
  b: string
  c: number
  vf: number
  vt?: number
}

export default function BiblePopover({
  refs,
}: Readonly<{ refs: BibleReference[] }>) {
  const content = (
    <PopoverContent className="w-full sm:w-96">
      <div className="w-full leading-6 p-2 max-h-64 overflow-x-auto">
        {refs.map((ref, i) => {
          const { b, c, vf, vt } = ref
          const length = vt ? vt - vf + 1 : 1
          const verseList = Array.from({ length }, (_, i) => vf + i)
          const verses = BiblePassageService.getVerses(b, c, verseList)
          if (verses.length === 0) {
            throw new Error(`No verses found for ${b} ${c}: ${vf}`)
          }
          return (
            <div key={i}>
              {i > 0 && <Divider className="mt-3 mb-3" />}
              <div className="flex justify-between items-center text-xs font-semibold text-slate-400">
                <span>
                  {b} {c}
                </span>
                <span className="text-xs text-slate-400">KJV</span>
              </div>
              {verses.map((verse, verseIndex) => (
                <div key={verseIndex}>
                  <span className="text-slate-400 text-xs">
                    vs {verseList[verseIndex]}:
                  </span>{' '}
                  {verse}
                </div>
              ))}
              <div>
                <Link
                  href={`https://biblia.com/bible/kjv1900/${b}/${c}`}
                  rel="noopener noreferrer"
                  target="_blank"
                  className="text-xs"
                  title="Read chapter at biblia.com"
                  showAnchorIcon
                >
                  Read Chapter
                </Link>
              </div>
            </div>
          )
        })}
      </div>
    </PopoverContent>
  )

  return (
    <Popover backdrop="opaque" placement="bottom">
      <PopoverTrigger>
        <Button
          size="sm"
          variant="light"
          color="primary"
          isIconOnly
          className="-mt-3 -mb-3"
        >
          <BookOpenText size="12" />
        </Button>
      </PopoverTrigger>
      {content}
    </Popover>
  )
}

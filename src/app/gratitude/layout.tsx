import type { Metadata } from 'next'
import '../globals.css'

export const metadata: Metadata = {
  title: 'Part 3: Gratitude - Guilt Grace Gratitude',
  description:
    'Part 3 of the Heidelberg Catechism describing the gratitude due from us for such a deliverance.',
}

export default function RootLayout({
  children,
}: Readonly<{ children: React.ReactNode }>) {
  return <>{children}</>
}

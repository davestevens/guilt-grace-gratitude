import type { Metadata } from 'next'
import '../globals.css'

export const metadata: Metadata = {
  title: 'About - Guilt Grace Gratitude',
  description:
    'About the Guilt Grace Gratitude website, why and by whom it was created as well as source materials.',
}

export default function RootLayout({
  children,
}: Readonly<{ children: React.ReactNode }>) {
  return <>{children}</>
}

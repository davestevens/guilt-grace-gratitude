# Guilt Grace Gratitude

This repo contains the source for the website guiltgracegratitude.org,
an illustrated edition of the Heidelberg Catechism.

## Dev Setup

```
npm i
npm run dev   # run dev server
npm run lint  # run eslint
npm run build # build static site
npm run serve # test static html
```

## Swag Ideas
- Rounded business cards
- Stickers
- Bumper Stickers
- T-Shirts

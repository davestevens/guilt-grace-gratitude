import React from 'react'
import { Grenze_Gotisch } from 'next/font/google'
import Link from 'next/link'

const grenze = Grenze_Gotisch({ weight: '400', subsets: ['latin'] })

export default function Header() {
  return (
    <div className="flex-col items-center">
      <Link href="/">
        <h1
          className={`${grenze.className} text-4xl sm:text-6xl text-center mb-1 mt-4`}
        >
          Guilt • Grace • Gratitude
        </h1>
      </Link>
      <h2 className="text-l sm:text-xl text-center text-slate-400 mb-1 sm:mb-2">
        An Illustrated Edition of the Heidelberg Catechism
      </h2>
    </div>
  )
}

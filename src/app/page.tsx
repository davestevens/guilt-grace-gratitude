import comfortInLifeImage from '../../public/images/comfort_in_life.webp'
import guiltImage from '../../public/images/guilt.webp'
import graceImage from '../../public/images/grace.webp'
import gratitudeImage from '../../public/images/graditude.webp'
import MaxWidthLayout from '@/components/max-width-layout'
import SectionSelectCard from '@/components/section-select-card'

export default function Home() {
  return (
    <MaxWidthLayout>
      <div className="flex flex-col place-items-center w-full">
        <SectionSelectCard
          image={comfortInLifeImage}
          heading="Comfort"
          subHeading="Our only comfort in life and death."
          questions="Questions 1 and 2"
          linkTo="/comfort"
        />
        <SectionSelectCard
          image={guiltImage}
          heading="Part 1: Guilt"
          subHeading="God’s perfect law shows us our guilt."
          questions="Questions 3-11"
          linkTo="/guilt"
        />
        <SectionSelectCard
          image={graceImage}
          heading="Part 2: Grace"
          subHeading="How we can be delivered from eternal punishment."
          questions="Questions 12-91"
          linkTo="/grace"
        />
        <SectionSelectCard
          image={gratitudeImage}
          heading="Part 3: Gratitude"
          subHeading="The gratitude due from us for such a deliverance."
          questions="Questions 92-129"
          linkTo="/gratitude"
        />
      </div>
    </MaxWidthLayout>
  )
}

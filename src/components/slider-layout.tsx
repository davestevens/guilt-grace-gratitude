import Link from 'next/link'
import MaxWidthLayout from '@/components/max-width-layout'
import { Button, Slider } from '@heroui/react'
import {
  ChevronLeft,
  ChevronRight,
  ChevronsLeft,
  ChevronsRight,
  List,
} from 'lucide-react'
import { Grenze_Gotisch } from 'next/font/google'
import React from 'react'
import { AnimatePresence } from 'framer-motion'
import { useDebouncedCallback } from 'use-debounce'

const grenze = Grenze_Gotisch({ weight: '400', subsets: ['latin'] })

export default function SliderLayout({
  title,
  minPage,
  maxPage,
  page,
  setPage,
  prevSection,
  prevSectionMobile,
  prevRoute,
  nextSection,
  nextSectionMobile,
  nextRoute,
  children,
}: Readonly<{
  title: string
  minPage: number
  maxPage: number
  page: number
  setPage: (page: number) => void
  prevSection?: string
  prevSectionMobile?: string
  prevRoute?: string
  nextSection?: string
  nextSectionMobile?: string
  nextRoute?: string
  children: React.ReactNode
}>) {
  const debouncedSetQString = useDebouncedCallback((newPageNumber: number) => {
    // This function is required to avoid getting the error
    // "Too many calls to Location or History APIs within a short timeframe."
    if (window.history.replaceState) {
      let url = window.location.protocol
      url += '//'
      url += window.location.host
      url += window.location.pathname
      url += `?q=${newPageNumber}`
      window.history.replaceState({ path: url }, '', url)
    }
  }, 200)

  const onNavPage = (newPageNumber: number) => {
    setPage(newPageNumber)
    debouncedSetQString(newPageNumber)
  }

  const onPageLoad = () => {
    const searchParams = new URLSearchParams(window.location.search)
    const question = searchParams.get('q')
    if (question) {
      setPage(Number.parseInt(question))
    }
  }

  React.useEffect(onPageLoad, [])

  return (
    <MaxWidthLayout>
      <div className="flex flex-col place-items-center mb-4 w-full">
        <div className="flex items-center w-full mb-4">
          <Button
            as={Link}
            href="/"
            color="primary"
            variant="bordered"
            radius="full"
            isIconOnly={true}
            className="flex sm:hidden mr-2"
            startContent={<List />}
          />
          <Button
            as={Link}
            href="/"
            color="primary"
            variant="bordered"
            radius="full"
            className="hidden sm:flex mr-2"
            startContent={<List />}
          >
            Contents
          </Button>
          {prevRoute && (
            <Button
              as={Link}
              href={prevRoute}
              color="primary"
              variant="bordered"
              radius="full"
              startContent={<ChevronsLeft />}
            >
              <span className="hidden sm:flex">{prevSection}</span>
              <span className="flex sm:hidden">{prevSectionMobile}</span>
            </Button>
          )}
          {nextRoute && (
            <Button
              as={Link}
              href={nextRoute}
              color="primary"
              variant="bordered"
              radius="full"
              className="ml-auto"
              startContent={<ChevronsRight />}
            >
              <span className="hidden sm:flex">{nextSection}</span>
              <span className="flex sm:hidden">{nextSectionMobile}</span>
            </Button>
          )}
        </div>
        <div className="flex items-center w-full mb-2">
          <h1
            className={`${grenze.className} leading-none text-2xl sm:text-3xl`}
          >
            {title}
          </h1>
          <div className="flex items-center ml-auto">
            <span className="mr-3">Question {page}</span>
            <Button
              color="primary"
              variant="bordered"
              radius="full"
              isIconOnly
              isDisabled={page === minPage}
              onPress={() => onNavPage(page - 1)}
            >
              <ChevronLeft />
            </Button>
            <Button
              color="primary"
              variant="bordered"
              radius="full"
              isIconOnly
              className="ml-2"
              isDisabled={page === maxPage}
              onPress={() => onNavPage(page + 1)}
            >
              <ChevronRight />
            </Button>
          </div>
        </div>
        <Slider
          size="md"
          step={1}
          label=" "
          value={page}
          onChange={(p) => onNavPage(p as number)}
          showTooltip={true}
          showSteps={true}
          maxValue={maxPage}
          minValue={minPage}
          defaultValue={minPage}
          hideValue={true}
          className="max-w"
        />
      </div>
      <AnimatePresence mode="sync">{children}</AnimatePresence>
    </MaxWidthLayout>
  )
}

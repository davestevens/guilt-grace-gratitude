import React from 'react'
import { Grenze_Gotisch } from 'next/font/google'

const grenze = Grenze_Gotisch({ weight: '400', subsets: ['latin'] })

export default function Question({
  children,
}: Readonly<{
  children: React.ReactNode
}>) {
  return (
    <div className="flex mb-2">
      <div className={`${grenze.className} text-4xl leading-none`}>Q.</div>{' '}
      <p className="mt-2 ml-2 text-slate-400">{children}</p>
    </div>
  )
}

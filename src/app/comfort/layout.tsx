import type { Metadata } from 'next'
import '../globals.css'

export const metadata: Metadata = {
  title: 'Our Comfort - Guilt Grace Gratitude',
  description:
    'The introductory two questions of the Heidelberg Catechism describing our only comfort in life and death.',
}

export default function RootLayout({
  children,
}: Readonly<{ children: React.ReactNode }>) {
  return <>{children}</>
}

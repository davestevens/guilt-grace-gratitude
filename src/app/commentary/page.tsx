'use client'

import { IReactReaderStyle, ReactReader, ReactReaderStyle } from 'react-reader'
import React from 'react'
import { Link, HeroUIProvider } from '@heroui/react'
import Header from '@/components/header'
import { Open_Sans } from 'next/font/google'

const openSans = Open_Sans({ weight: '300', subsets: ['latin'] })
const darkThemeBg = '#09090b'
const darkThemeBg1 = '#131317'
const darkThemeBg2 = '#1b1b20'
const darkThemeColor = '#9ca3af'
const theme: IReactReaderStyle = {
  ...ReactReaderStyle,
  container: { ...ReactReaderStyle.container, width: '100%' },
  arrow: {
    ...ReactReaderStyle.arrow,
    color: darkThemeColor,
  },
  arrowHover: {
    ...ReactReaderStyle.arrowHover,
    color: darkThemeColor,
  },
  readerArea: {
    ...ReactReaderStyle.readerArea,
    backgroundColor: darkThemeBg,
    color: darkThemeColor,
    transition: undefined,
  },
  titleArea: {
    ...ReactReaderStyle.titleArea,
    color: darkThemeColor,
  },
  tocArea: {
    ...ReactReaderStyle.tocArea,
    background: darkThemeBg1,
  },
  tocButtonExpanded: {
    ...ReactReaderStyle.tocButtonExpanded,
    background: darkThemeBg2,
  },
  tocButtonBar: {
    ...ReactReaderStyle.tocButtonBar,
    background: darkThemeColor,
  },
  tocButton: {
    ...ReactReaderStyle.tocButton,
    color: darkThemeColor,
  },
}

const eBookUrl = 'epub/HeidelbergCatechismCommentaryByZachariasUrsinus.epub'

export default function EpubViewer() {
  const [location, setLocation] = React.useState<string | number>(0)

  const onPageLoad = () => {
    const searchParams = new URLSearchParams(window.location.search)
    const file = searchParams.get('f')
    const question = searchParams.get('q')
    if (file && question) {
      setLocation(`commentrayhc_split_${file}.html#q${question}`)
    }
  }

  React.useEffect(onPageLoad, [])
  return (
    <html lang="en" className="dark">
      <body
        className={`${openSans.className} dark bg-zinc-950 text-slate-50 flex flex-col w-full min-h-screen`}
      >
        <HeroUIProvider className="flex flex-col items-center">
          <main className="flex flex-col items-center w-full h-screen">
            <Header />
            <div className="flex flex-col sm:flex-row justify-between align-middle w-full pt-6 pl-4 pb-2 pr-4">
              <h2 className="text-sm text-slate-400">
                Commentary on the Heidelberg Catechism by Dr. Zacharias Ursinus
              </h2>
              <Link href={eBookUrl} className="text-sm">
                EPUB eBook
              </Link>
            </div>
            <div className="flex flex-col w-full h-full items-center pl-4 pr-4 pb-4">
              <div className="flex w-full h-full border-1 border-gray-700 rounded overflow-hidden">
                <ReactReader
                  url={eBookUrl}
                  location={location}
                  locationChanged={(epubcfi: string) => {
                    setLocation(epubcfi)
                  }}
                  readerStyles={theme}
                  getRendition={(_rendition) => {
                    const themes = _rendition.themes
                    themes.override('color', darkThemeColor)
                    themes.override('background', darkThemeBg)
                  }}
                />
              </div>
            </div>
          </main>
        </HeroUIProvider>
      </body>
    </html>
  )
}

import passageData from '../build/passages.json'

const passages = passageData as Passages

export interface Passages {
  [book: string]: { [chapter: string]: { [verse: string]: string } }
}

export default class BiblePassageService {
  public static getVerses(
    book: string,
    chapter: number,
    verses: number[],
  ): string[] {
    const bk = passages[book]
    if (!bk) return []
    const ch = bk[`${chapter}`]
    if (!ch) return []
    return verses.map((vs) =>
      BiblePassageService.trimParagraphMark(ch[`${vs}`]),
    )
  }

  private static trimParagraphMark(vs: string): string {
    return vs?.replace(/^# /, '')
  }
}

import React from 'react'
import ExportedImage from 'next-image-export-optimizer'
import { StaticImageData } from 'next/image'
import { motion } from 'framer-motion'
import { Modal, ModalBody, ModalContent, ModalHeader } from '@heroui/modal'

export default function QuestionAnswerGroup({
  image,
  currentPage,
  showOnPage,
  imageAlt,
  children,
}: Readonly<{
  image: StaticImageData
  currentPage: number
  showOnPage: number
  imageAlt: string
  children: React.ReactNode
}>) {
  if (currentPage !== showOnPage) return null

  const [isOpen, setIsOpen] = React.useState(false)

  const onImageClick = () => {
    // Not opening zoom dialog on mobile because the image is already 100% width
    if (window.innerWidth < 640) {
      return
    }
    setIsOpen(true)
  }

  return (
    <>
      <motion.div
        key={showOnPage}
        className="flex w-full flex-col sm:flex-row"
        initial={{ y: 20, opacity: 0 }}
        animate={{ y: 0, opacity: 1 }}
        exit={{ y: -20, opacity: 0 }}
        transition={{ duration: 0.5 }}
      >
        <div className="flex flex-col flex-shrink-0">
          <ExportedImage
            src={image}
            alt={imageAlt}
            className="w-full sm:w-80 sm:h-80 rounded-md sm:cursor-zoom-in"
            onClick={() => onImageClick()}
          />
          <div className="text-sm mt-1 text-gray-500 w-full sm:w-80 sm:h-80">
            {imageAlt}
          </div>
        </div>
        <div className="sm:ml-4 mt-3 sm:mt-0 text-lg">{children}</div>
      </motion.div>
      <Modal isOpen={isOpen} onClose={() => setIsOpen(false)} size="3xl">
        <ModalContent>
          <ModalHeader className="flex flex-col gap-1">{imageAlt}</ModalHeader>
          <ModalBody>
            <ExportedImage
              src={image}
              alt={imageAlt}
              className="w-full rounded mb-4"
            />
          </ModalBody>
        </ModalContent>
      </Modal>
    </>
  )
}

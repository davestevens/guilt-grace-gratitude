import React from 'react'
import { Grenze_Gotisch } from 'next/font/google'
import ExportedImage from 'next-image-export-optimizer'
import { StaticImageData } from 'next/image'
import Link from 'next/link'

const grenze = Grenze_Gotisch({ weight: '400', subsets: ['latin'] })

export default function SectionSelectCard({
  image,
  heading,
  subHeading,
  questions,
  linkTo,
}: Readonly<{
  image: StaticImageData
  heading: string
  subHeading: string
  questions: string
  linkTo: string
}>) {
  return (
    <Link
      href={linkTo}
      className="flex w-full rounded-lg overflow-hidden bg-slate-900 hover:bg-slate-800 mb-4 last:mb-0"
    >
      <ExportedImage
        src={image}
        alt="Comfort In Life"
        className="w-32 sm:w-40 h-32 sm:h-40"
      />
      <div className="flex flex-col p-4 w-full text-small sm:text-lg leading-5">
        <div className="flex flex-col sm:flex-row sm:justify-between sm:items-center w-full">
          <h3
            className={`${grenze.className} leading-none text-3xl sm:text-4xl`}
          >
            {heading}
          </h3>
          <div className="text-slate-400">{questions}</div>
        </div>
        <div className="text-slate-400">{subHeading}</div>
      </div>
    </Link>
  )
}

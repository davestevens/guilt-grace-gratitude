'use client'

import React from 'react'
import burdenImage from '../../../public/images/burden.webp'
import goodSamaritanImage from '../../../public/images/good_samaritan.webp'
import rageImage from '../../../public/images/rage.webp'
import guiltImage from '../../../public/images/guilt.webp'
import adamImage from '../../../public/images/adam.webp'
import inabilityImage from '../../../public/images/inability.webp'
import rebellionImage from '../../../public/images/rebellion.webp'
import displeasedImage from '../../../public/images/displeased.webp'
import fireImage from '../../../public/images/lake_of_fire.webp'
import SliderLayout from '@/components/slider-layout'
import QuestionAnswerGroup from '@/components/question-answer-group'
import Question from '@/components/question'
import Answer from '@/components/answer'
import BiblePopover from '@/components/bible-popover'
import Commentary from '@/components/commentary'
import LordsDay from '@/components/lords-day'

export default function Guilt() {
  const minPage = 3
  const maxPage = 11
  const [page, setPage] = React.useState(minPage)

  return (
    <SliderLayout
      title="Part 1: Guilt"
      minPage={minPage}
      maxPage={maxPage}
      page={page}
      setPage={setPage}
      prevSection="Comfort"
      prevSectionMobile="Comfort"
      prevRoute="/comfort"
      nextSection="Part 2: Grace"
      nextSectionMobile="2: Grace"
      nextRoute="/grace"
    >
      <QuestionAnswerGroup
        image={burdenImage}
        currentPage={page}
        showOnPage={3}
        key={3}
        imageAlt="Burden"
      >
        <LordsDay day={2} />
        <Question>From where do you know your misery?</Question>
        <Answer>
          From the Law of God.
          <BiblePopover
            refs={[
              { b: 'Romans', c: 3, vf: 20 },
              { b: 'Romans', c: 7, vf: 7 },
            ]}
          />
        </Answer>
        <Commentary f="003" q="003" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={goodSamaritanImage}
        currentPage={page}
        showOnPage={4}
        key={4}
        imageAlt="The Good Samaritan"
      >
        <Question>What does the Law of God require of us?</Question>
        <Answer>
          Christ teaches us in sum, Matthew 22, “Thou shalt love the Lord thy
          God with all thy heart, and with all thy soul, and with all thy mind.
          This is the first and great commandment. And the second [is] like unto
          it, Thou shalt love thy neighbour as thyself. On these two
          commandments hang all the law and the prophets.”
          <BiblePopover
            refs={[
              { b: 'Matthew', c: 22, vf: 37, vt: 40 },
              { b: 'Luke', c: 10, vf: 27 },
              { b: 'Deuteronomy', c: 6, vf: 5 },
              { b: 'Galatians', c: 5, vf: 14 },
            ]}
          />
        </Answer>
        <Commentary f="003" q="004" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={rageImage}
        currentPage={page}
        showOnPage={5}
        key={5}
        imageAlt="Rage"
      >
        <Question>Can you keep all this perfectly?</Question>
        <Answer>
          No,
          <BiblePopover
            refs={[
              { b: 'Romans', c: 3, vf: 10, vt: 12 },
              { b: 'Romans', c: 3, vf: 23 },
              { b: '1 John', c: 1, vf: 8 },
              { b: '1 John', c: 1, vf: 10 },
            ]}
          />
          for I am prone by nature to hate God and my neighbor.
          <BiblePopover
            refs={[
              { b: 'Romans', c: 8, vf: 7 },
              { b: 'Ephesians', c: 2, vf: 3 },
            ]}
          />
        </Answer>
        <Commentary f="003" q="005" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={adamImage}
        currentPage={page}
        showOnPage={6}
        key={6}
        imageAlt="Adam"
      >
        <LordsDay day={3} />
        <Question>Did God create man thus, wicked and perverse?</Question>
        <Answer>
          No,
          <BiblePopover refs={[{ b: 'Genesis', c: 1, vf: 31 }]} />
          but God created man good and after His own image,
          <BiblePopover refs={[{ b: 'Genesis', c: 1, vf: 26, vt: 27 }]} />
          that is, in righteousness and true holiness, that he might rightly
          know God his Creator, heartily love Him, and live with Him in eternal
          blessedness, to praise and glorify Him.
          <BiblePopover
            refs={[
              { b: '2 Corinthians', c: 3, vf: 18 },
              { b: 'Colossians', c: 3, vf: 10 },
              { b: 'Ephesians', c: 4, vf: 24 },
            ]}
          />
        </Answer>
        <Commentary f="003" q="006" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={guiltImage}
        currentPage={page}
        showOnPage={7}
        key={7}
        imageAlt="Guilt"
      >
        <Question>
          From where, then, does this depraved nature of man come?
        </Question>
        <Answer>
          From the fall and disobedience of our first parents, Adam and Eve, in
          Paradise,
          <BiblePopover
            refs={[
              { b: 'Genesis', c: 3, vf: 6 },
              { b: 'Romans', c: 5, vf: 12 },
              { b: 'Romans', c: 5, vf: 18, vt: 19 },
            ]}
          />
          whereby our nature became so corrupt that we are all conceived and
          born in sin.
          <BiblePopover
            refs={[
              { b: 'Psalms', c: 51, vf: 5 },
              { b: 'Psalms', c: 14, vf: 2, vt: 3 },
            ]}
          />
        </Answer>
        <Commentary f="003" q="007" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={inabilityImage}
        currentPage={page}
        showOnPage={8}
        key={8}
        imageAlt="Inability"
      >
        <Question>
          But are we so depraved that we are completely incapable of any good
          and prone to all evil?
        </Question>
        <Answer>
          Yes,
          <BiblePopover
            refs={[
              { b: 'John', c: 3, vf: 6 },
              { b: 'Genesis', c: 6, vf: 5 },
              { b: 'Job', c: 14, vf: 4 },
              { b: 'Isaiah', c: 53, vf: 6 },
            ]}
          />
          unless we are born again by the Spirit of God.
          <BiblePopover
            refs={[
              { b: 'John', c: 3, vf: 5 },
              { b: 'Genesis', c: 8, vf: 21 },
              { b: '2 Corinthians', c: 3, vf: 5 },
              { b: 'Romans', c: 7, vf: 18 },
              { b: 'Jeremiah', c: 17, vf: 9 },
            ]}
          />
        </Answer>
        <Commentary f="003" q="008" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={rebellionImage}
        currentPage={page}
        showOnPage={9}
        key={9}
        imageAlt="Deprived Rebels"
      >
        <LordsDay day={4} />
        <Question>
          Does not God, then, do injustice to man by requiring of him in His Law
          that which he cannot perform?
        </Question>
        <Answer>
          No, for God so made man that he could perform it;
          <BiblePopover refs={[{ b: 'Ephesians', c: 4, vf: 24 }]} />
          but man, through the instigation of the devil, by willful disobedience
          deprived himself and all his descendants of those divine gifts.
          <BiblePopover refs={[{ b: 'Romans', c: 5, vf: 12 }]} />
        </Answer>
        <Commentary f="003" q="009" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={displeasedImage}
        currentPage={page}
        showOnPage={10}
        key={10}
        imageAlt="Terribly Displeased"
      >
        <Question>
          Will God allow such disobedience and apostasy to go unpunished?
        </Question>
        <Answer>
          Certainly not,
          <BiblePopover refs={[{ b: 'Hebrews', c: 9, vf: 27 }]} />
          but He is terribly displeased with our inborn as well as our actual
          sins, and will punish them in just judgment in time and eternity, as
          He has declared, “Cursed [is] every one that continueth not in all
          things which are written in the book of the law to do them.”
          <BiblePopover
            refs={[
              { b: 'Deuteronomy', c: 27, vf: 26 },
              { b: 'Galatians', c: 3, vf: 10 },
              { b: 'Romans', c: 1, vf: 18 },
              { b: 'Matthew', c: 25, vf: 41 },
            ]}
          />
        </Answer>
        <Commentary f="003" q="010" />
      </QuestionAnswerGroup>
      <QuestionAnswerGroup
        image={fireImage}
        currentPage={page}
        showOnPage={11}
        key={11}
        imageAlt="Lake of Fire"
      >
        <Question>But is not God also merciful?</Question>
        <Answer>
          God is indeed merciful,
          <BiblePopover refs={[{ b: 'Exodus', c: 34, vf: 6, vt: 7 }]} />
          but He is likewise just;
          <BiblePopover
            refs={[
              { b: 'Exodus', c: 20, vf: 5 },
              { b: 'Psalms', c: 5, vf: 5, vt: 6 },
              { b: '2 Corinthians', c: 6, vf: 14, vt: 16 },
              { b: 'Revelation', c: 14, vf: 11 },
            ]}
          />
          His justice therefore requires that sin which is committed against the
          most high majesty of God, be punished with extreme, that is, with
          everlasting punishment both of body and soul.
        </Answer>
        <Commentary f="003" q="011" />
      </QuestionAnswerGroup>
    </SliderLayout>
  )
}

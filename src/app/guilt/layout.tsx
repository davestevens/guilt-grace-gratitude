import type { Metadata } from 'next'
import '../globals.css'

export const metadata: Metadata = {
  title: 'Part 1: Guilt - Guilt Grace Gratitude',
  description:
    'Part 1 of the Heidelberg Catechism describing how God’s perfect law reveals to us our guilt.',
}

export default function RootLayout({
  children,
}: Readonly<{ children: React.ReactNode }>) {
  return <>{children}</>
}
